<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page title</title>
	<link rel="stylesheet" href="includes/styles.css">

    <?php
        //pagination
            if (isset($_GET['pageno'])) {
                $pageno = $_GET['pageno'];
            }
            else {
                $pageno = 1;
            }

            $no_of_records_per_page = 40;
            $offset = ($pageno-1) * $no_of_records_per_page; 
            include("../live_connect/connect.inc");

        //search queries
        if(isset($_POST['vemail'])) {
           
            $total_pages_sql = "SELECT COUNT(*) FROM tickets_tickets";
            $result = mysqli_query($conn,$total_pages_sql);
            $total_rows = mysqli_fetch_array($result)[0];
            $total_pages = ceil($total_rows / $no_of_records_per_page);

            $sql = "SELECT * FROM tickets_newtickets WHERE email = '".$_POST['vemail']."' ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";

        }

    ?>
	
    <style>
        table {
            border: 1px solid lightgrey;
        }
        .tdh {
            text-align:left;
            color: #FFFFFF;
            background-color: #013245;
            border: none;
            padding-left: 5px;
        }
        .tdc {
            border: solid lightgray;
            border-width: 1px 1px 0px 0px;
            padding: 5px 5px;
        }
        .dheader {
            display: none;

        }
        .details {
            display: none;
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function (){
            $(".expand~label").hover(function (){
                $(this).css("background-color", "lightgrey");
                }, function (){
                $(this).css("background-color", "grey");
            });

            

        <?php
            include("../live_connect/connect.inc");
            $dsql = "SELECT id FROM tickets_newtickets WHERE email = '".$_POST['vemail']."'";
            $q = mysqli_query($conn, $dsql); 
            while($r = mysqli_fetch_array($q)) {
        ?>
            $("#expand<?php echo $r['id'] ?>").click(function () {
                if($(this).prop("checked") == false) {
                    $("#detail<?php echo $r['id'] ?>").css("display", "none");
                    $("#dheader<?php echo $r['id'] ?>").css("display", "none");
                    $("#dtheader<?php echo $r['id'] ?>").css("display", "none");
                    $("#dt<?php echo $r['id'] ?>").css("display", "none");
                    $("#adinfoheader<?php echo $r['id'] ?>").css("display", "none");
                    $("#adinfo<?php echo $r['id'] ?>").css("display", "none");
                }
                else if($(this).prop("checked") == true) {
                    $("#detail<?php echo $r['id'] ?>").css("display", "table-row");
                    $("#dheader<?php echo $r['id'] ?>").css("display", "table-row");
                    $("#dtheader<?php echo $r['id'] ?>").css("display", "table-row");
                    $("#dt<?php echo $r['id'] ?>").css("display", "table-row");
                    $("#adinfoheader<?php echo $r['id'] ?>").css("display", "table-row");
                    $("#adinfo<?php echo $r['id'] ?>").css("display", "table-row");
                }
            });

        <?php
            }
        ?>
        });
        
    </script>
      
</head>
<body id='main_page'>
    <nav class='menu'>
		<?php include_once('includes/menu.php') ?>
	</nav>
    <content class='iframe' id='content'>

    <?php 
        if(isset($_POST['vemail'])) {
    ?>
       
        <div class='headingArea'>My Tickets</div>
        <div class='page'>
        <h3></h3>
        <table cellspacing='0' cellpadding='0' class='iframe_table' id='table'>
            <tr>
                <th class='tdh' style='width:10px;'>ID</th>
                <th class='tdh'>Name</th>
                <th class='tdh'>Class</th>
                <th class='tdh'>Page</th>
                <th class='tdh'>Week</th>
                <th class='tdh'>Issue</th>
                <th class='tdh'>Submitted Date</th>
                <th class='tdh'>Expand</th>
            </tr>

        <?php
        
        
        include("../live_connect/connect.inc");
        $query = mysqli_query($conn, $sql);
        while($rows = mysqli_fetch_array($query)) {
            if($rows['submitteddate'] == '0000-00-00') {
                $sdate = '';
            }
            else {
                $sdate = strtotime($rows['submitteddate']);
                $sdate = date('d-M-Y', $sdate);
            }
            if($rows['assigneddate'] == '0000-00-00') {
                $adate = '';
            }
            else {
                $adate = strtotime($rows['assigneddate']);
                $adate = date('d-M-Y',$adate);
            }
            if($rows['duedate'] == '0000-00-00') {
                $ddate = '';
            }
            else {
                $ddate = strtotime($rows['duedate']);
                $ddate = date('d-M-Y', $ddate);
            }
            if($rows['completeddate'] == '0000-00-00') {
                $cdate = '';
            }
            else {
                $cdate = strtotime($rows['completeddate']);
                $cdate = date('d-M-Y', $cdate);
            }

            echo "
                <tr>
                    <td class='tdc'>".$rows['id']."</td>
                    <td class='tdc'>";
                    $pos = strpos($rows['fullname'], '.');

                        if($pos > 0 || $pos != '') {
                            $name = explode('.', $rows['fullname']);
                        }
                        else {
                            $name = explode(' ', $rows['fullname']);
                        }
                    
                   echo ucfirst($name[0]) . " " .ucfirst($name[1])."</td> ";
                    if(strlen($rows['class']) > 6) {
                        $class = substr($rows['class'], 0, 6);
                    }
                    else {
                        $class = $rows['class'];
                    }
                    echo "
                    <td class='tdc'>".$class."</td>
                    <td class='tdc'>".$rows['page']."</td>
                    <td class='tdc'>".$rows['week']."</td>
                    <td class='tdc'>".$rows['issue']."</td>
                    <td class='tdc'>".$sdate."</td>
                    <td class='tdc' style='border-right: 0px;'>
                        <input type='checkbox' id='expand".$rows['id']."' class='expand' value='expand".$rows['id']."'/>
                            <label for='expand".$rows['id']."'>Expand</label>
                    </td>
                </tr>
                <tr class='dheader' id='adinfoheader".$rows['id']."'>
                    <td></td>
                    <td class='tdh'>Course URL</td>
                    <td class='tdh' colspan='2'>Course Name</td>
                    <td class='tdh'>Program</td>
                    <td class='tdh'>Tier</td>
                    <td class='tdh'>Agent</td>
                    <td class='tdh'>Status</td>
                </tr>
                <tr class='details' id='adinfo".$rows['id']."'>
                    <td></td>
                    <td class='tdc' style='border-left: 1px;'>".$rows['courseurl']."</td>
                    <td class='tdc' colspan='2'>".$rows['cname']."</td>
                    <td class='tdc'>";
                    switch($rows['program']) {
                        case "AH":
                            echo "Healthcare";
                        break;
                        case "B":
                            echo "Business and Accounting";
                        break;
                        case "B/A":
                            echo "Business and Accounting";
                        break;
                        case "B?A":
                            echo "Business and Accounting";
                        break;
                        case "BIS":
                            echo "Business and Accounting";
                        break;
                        case "CS":
                            echo "Computer Science";
                        break;
                        case "DA":
                            echo "Graphic Arts";
                        break;
                        case "DES209":
                            echo "Graphic Arts";
                        break;
                        case "GA":
                            echo "Graphic Arts";
                        break;
                        case "GD":
                            echo "Graphic Arts";
                        break;
                        case "GE":
                            echo "General Education";
                        break;
                        case "GenEd":
                            echo "General Education";
                        break;
                        case " GW":
                            echo "Graphic Arts";
                        break;
                        case "HC":
                            echo "Healthcase";
                        break;
                        case "IT":
                            echo "Technology";
                        break;
                        case "RT":
                            echo "Respiratory Therapy";
                        break;
                        default:
                            echo $rows['program'];
                    }
                    echo "</td>
                    <td class='tdc'>".$rows['tier']."</td>
                    <td class='tdc'>";
                        $agent = explode(".", $rows['agent']);
                        $agent = implode(" ", $agent);
                        $agent = ucwords($agent);
                        echo $agent;
                    echo "</td>
                    <td class='tdc' style='border-bottom: 1px;'>".$rows['status']."</td>
                </tr>
                <tr id='dtheader".$rows['id']."' class='dheader'>
                    <td></td>
                    <td class='tdh' colspan='3'>Associated Degrees</td>
                    <td class='tdh'>Assigned Date</td>
                    <td class='tdh'>Due Date</td>
                    <td class='tdh' colspan='2'>Completed Date</td>
                </tr>
                <tr id='dt".$rows['id']."' class='details'>
                    <td></td>
                    <td class='tdc' colspan='3'>".$rows['degrees']."</td>
                    <td class='tdc'>".$adate."</td>
                    <td class='tdc'>".$ddate."</td>
                    <td class='tdc' colspan='2'>".$cdate."</td>
                </tr>
                <tr id='dheader".$rows['id']."' class='dheader'>
                    <td></td>
                    <td class='tdh' colspan='3'>Details</td>
                    <td class='tdh' colspan='4'>Final Notes</td>
                </tr>
                <tr id='detail".$rows['id']."' class='details' style='border-bottom: 1px;'>
                    <td class='tdc' style='border-top: 0px;'></td>
                    <td class='tdc' colspan='3'>".$rows['user_notes']."</td>
                    <td class='tdc' colspan='4'>".$rows['final_notes']."</td>
                </tr>
            ";
            }
            echo "</table>";
       ?>
        </div>
        <?php } else { ?>
        <div class="headingArea">My Tickets</div>
            <div class="page">
            <h3>Enter the email that you used to submit your ticket requests below.</h3>
            <form method='POST'>
                Email: <input type="text" name="vemail" >
                <input type="submit" value="Submit">
            </form>
            </div>
        <?php } ?>

    </content>
</body>
</html>

