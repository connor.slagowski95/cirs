<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Page title</title>
        
        <link rel="stylesheet" href="includes/styles.css">

        <style>
            .notes p {
                margin-left: 15px;
            }
        </style>

    </head>

    <body id='main_page'>
        <nav class='menu'>
            <?php include_once('includes/menu.php') ?>
        </nav>

        <content id='content'>
            <div class='headingArea'>Welcome to the Course Reporting Improvement System</div>
            
            <div style = 'grid-row: 2/-1; grid-column: 1/-1; margin: 0px 25px;' class='page'>
                <p>Here is a page by page break down of the ticket system: <p>
                <div class='notes' style='margin-left: 20px;'>
                    <h3>New Ticket</h3>
                        <p>The <strong>New Ticket</strong> page is where you will go to submit new tickets to the Curriculum Development Team (CDT) for issues that may 
                        be found in our courses.
                            You will fill out the information that the CDT needs, including first and last name, email, some information about the particular
                            class that the issue was found in, and some notes about the issue. This information will help us find the issue when we get to it.
                            After your ticket is submitted, we will use the email that you register with the ticket to send you updates when the status of your 
                            ticket changes, and you will also use that same email to log in when you want to view your own tickets.
                        </p>
                    <h3>My Tickets</h3>
                        <p>The <strong>My Tickets</strong> page is where you can look at a simple list of all the tickets you have that are still active in our system. 
                        This list will include some information at first glance with the capability to look in higher detail when the "Expand" button is clicked.
                        </p>
                </div>
            </div>
        </content>
    </body>
</html>

