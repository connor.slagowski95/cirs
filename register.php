<?php
    if(isset($_POST['vcode'])) {

        if($_POST['vcode'] == $_POST['nvc']) {
            $cookie_name = 'email';
            $cookie_value = $_POST['email'];
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

            $cookie_name = 'code';
            $cookie_value = $_POST['nvc'];
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

            echo "<meta http-equiv='refresh' content='0; url=".$_GET['page'].".php'>";
        }
    }

    if(isset($_POST['email'])) {
    
        $to = $_POST['email'];
        $subject = 'Verification Code';
        
        $msg = 
        "<html>
            <head>
                <title>HTML email</title>
            </head>
            <body>
                Thank you for helping the IU CDT better our courses. Your access code lasts for 24 hours before you will need to resubmit
                for another code.

                Here is your code: ".$_POST['code']."
            </body>
        </html>";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: no-reply@iucdt.com";


        mail($to, $subject, $msg, $headers);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page title</title>
    <link rel="stylesheet" href="includes/styles.css">
</head>
<body id='main_page'>
    <nav class='menu'>
		<?php include_once('includes/menu.php') ?>
	</nav>
    
<content class='iframe' id='content'>
    
<?php
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $code = substr(str_shuffle($permitted_chars), 0, 10);

    if(isset($_POST['email'])) {
        
        include("../../live_connect/connect.inc");
        $query = mysqli_query($conn, "SELECT DISTINCT email FROM tickets_newtickets");
        while($rows = mysqli_fetch_array($query)) {
            if($rows['email'] == $_POST['email']) {
                echo "
                    <div class='headingArea'>Enter the Verification Code</div>
                    <div class='page'>
                    <p>You were sent an email with a verification code. Enter the code in the field below.</p>
                    <form method='POST'>
                        <input style='font-size: 14pt;' type='text' name='vcode' required/>
                        <input type='hidden' name='nvc' value='".$_POST['code']."'/>
                        <input type='hidden' name='email' value='".$_POST['email']."' />
                        <input type='submit' value='Submit'/>
                        
                    </form>
                    </div>
                ";
            }
        }
    }
    else {
        
        echo "
            <div class='headingArea'>Email needed for verification</div>
            <div class='page'>
            <p style='font-size: 14pt; font-weight: bold;'>Enter the email that you used when you submitted your ticket.</p>
        
            <p style='font-size: 14pt;'>After submitting, if your email matches our records,
                you will <br />receive an email with a code that will allow access to this page.
            </p>
            
            <form method='POST'>
                Email: <input type='text' name='email' required>
                <input type='hidden' name='code' value='$code'/>
                <input type='submit' value='Submit'>
            </form>
            </div>
        ";
    }
?>
    
    </content>
</body>
</html>