<?php 
session_start([
    'cookie_lifetime' => 86400,
]); 

global $filename1, $filename2, $filename3;



//include("includes/security.inc");
//security("homepage");
//include("includes/header.inc");
?>
<!DOCTYPE html>
<html lang="en">


<head>
<link rel="stylesheet" href="includes/styles.css">
</head>

<?php
//Upload Images

if(isset($_POST['class_code'])) {	



	$allowf1 = '0';
	$allowf2 = '0';
	$allowf3 = '0';
	
	$sdate = date('d-m-Y-h-i-s-A');

	$target_file1 = basename($_FILES['fileToUpload1']['name']);
	$target_file2 = basename($_FILES['fileToUpload2']['name']);
	$target_file3 = basename($_FILES['fileToUpload3']['name']);
	
	// check to see if the file was uploaded
	if($target_file1 != '') {
		$allowf1 = '1';
	}

	if($target_file2 != '') {
		$allowf2 = '1';
	}

	if($target_file3 != '') {
		$allowf3 = '1';
	}
	
	
	
	// file 1 check 
	$file_type1 = pathinfo($target_file1, PATHINFO_EXTENSION);
				
	if($file_type1 != "docx" && $file_type1 != "doc" && $file_type1 != "jpg" && $file_type1 != "JPG"
	 && $file_type1 != "jpeg" && $file_type1 != "JPEG" && $file_type1 != "gif" && $file_type1 != "GIF" && $file_type1 != "pdf"
	 && $file_type1 != "PDF" && $file_type1 != "png" && $file_type1 != "PNG" && $file_type1 != "XD" && $file_type1 != "xd" && $file_type1 != "xlsx" && $file_type1 != "xlsb" && $file_type1 != "xltx" && $file_type1 != "csv" && $file_type1 != "CSV" && $file_type1 != "zip" && $file_type1 != "ZIP") {
		$allowf1 = '0';
	}
	
	if($allowf1 == '1') {
		$filename1 = pathinfo($target_file1, PATHINFO_FILENAME);
		$newFilename1 = $sdate . "-F1" . "." . $file_type1;
		$filelocation1 = "uploads/" . $newFilename1;
		
		move_uploaded_file($_FILES['fileToUpload1']['tmp_name'], $filelocation1);
		
		$file1 = $newFilename1;
	}
	elseif($allowf1 == '0') {
		$file1 = "";
		$_SESSION['msg1'] = "<div class='headingArea'>The file named: ".$filename1." was rejected. Please upload only the following filetypes (jpg, gif, png, docx, doc, pdf)</div>";
	}
	
	// file 2 check 
	$file_type2 = pathinfo($target_file2, PATHINFO_EXTENSION);
		
	if($file_type2 != "docx" && $file_type2 != "doc" && $file_type2 != "jpg" && $file_type2 != "JPG"
	 && $file_type2 != "jpeg" && $file_type2 != "JPEG" && $file_type2 != "gif" && $file_type2 != "GIF" && $file_type2 != "pdf"
	 && $file_type2 != "PDF" && $file_type2 != "png" && $file_type2 != "PNG" && $file_type2 != "XD" && $file_type2 != "xd" && $file_type2 != "xlsx" && $file_type2 != "xlsb" && $file_type2 != "xltx" && $file_type2 != "csv" && $file_type2 != "CSV" && $file_type2 != "zip" && $file_type2 != "ZIP") {
		$allowf2 = '0';
	}
	
	if($allowf2 == '1') {
		$filename2 = pathinfo($target_file2, PATHINFO_FILENAME);
		$newFilename2 = $sdate . "-F2" . "." . $file_type2;
		$filelocation2 = "uploads/" . $newFilename2;
		move_uploaded_file($_FILES['fileToUpload2']['tmp_name'], $filelocation2);
		
		$file2 = $newFilename2;
	}
	elseif($allowf2 == '0') {
		$file2 = "";
		$_SESSION['msg2'] = "<div class='headingArea'>The file named: ".$filename2." was rejected. Please upload only the following filetypes (jpg, gif, png, docx, doc, pdf)</div>";
	}

	// file 3 check 
	$file_type3 = pathinfo($target_file3, PATHINFO_EXTENSION);
		
	if($file_type3 != "docx" && $file_type3 != "doc" && $file_type3 != "jpg" && $file_type3 != "JPG"
	 && $file_type3 != "jpeg" && $file_type3 != "JPEG" && $file_type3 != "gif" && $file_type3 != "GIF" && $file_type3 != "pdf"
	 && $file_type3 != "PDF" && $file_type3 != "png" && $file_type3 != "PNG" && $file_type3 != "XD" && $file_type3 != "xd" && $file_type3 != "xlsx" && $file_type3 != "xlsb" && $file_type3 != "xltx" && $file_type3 != "csv" && $file_type3 != "CSV" && $file_type3 != "zip" && $file_type3 != "ZIP") {
		$allowf3 = '0';
	}
		
	if($allowf3 == '1') {		
		$filename3 = pathinfo($target_file3, PATHINFO_FILENAME);
		$newFilename3 = $sdate . "-F3" . "." . $file_type3;
		$filelocation3 = "uploads/" . $newFilename3;	
		move_uploaded_file($_FILES['fileToUpload3']['tmp_name'], $filelocation3);
		
		$file3 = $newFilename3;
	}
	elseif($allowf3 == '0') {
		$file3 = "";
		$_SESSION['msg3'] = "<div class='headingArea'>The file named: ".$filename3." was rejected. Please upload only the following filetypes (jpg, gif, png, docx, doc, pdf)</div>";
	}

	// insert data from ticket into database table
	
	include("../live_connect/connect.inc");
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $fullname = $fname . " "  . $lname;
	$email = $_POST['email'];
	$class_code = $_POST['class_code'];
	$page = $_POST['page'];
	$week = $_POST['week'];
	$issue = $_POST['issue'];
	$url = $_POST['curl'];
	
	$query = mysqli_query($conn, "SELECT tier FROM tickets_issues WHERE issue = '".$issue."'");
	while($rows = mysqli_fetch_array($query)) {
		$tier = $rows['tier'];
	}
	$statement = "SELECT * FROM tickets_newclasses WHERE classId = '".$class_code."'";
	$query = mysqli_query($conn, $statement) OR DIE("HAHA");
	while($rows = mysqli_fetch_array($query)) {
		$pvcodes = $rows['pvcodes'];
		$classId = $rows['classId'];
		$className = $rows['className'];
	}

	$pvcodes = explode(',', $pvcodes);
	$degrees = '';
	for($i=0; $i < count($pvcodes); $i++) {
		$q = mysqli_query($conn, "SELECT * FROM PV WHERE pvcode = '".$pvcodes[$i]."'");
		while($r = mysqli_fetch_array($q)) {
			if($degrees == '') {
				$degrees = $r['degree'];
			}
			else {
				$degrees = $degrees . ', ' . $r['degree'];
			}
			$program = $r['program'];
		}
	}

	$pvcodes = implode(", ", $pvcodes);
	$note = $_POST['note'];
	$priority = '';
	$submitteddate = date("Y-m-d");
	$status = 'Pending';

	$statement = "INSERT INTO tickets_newtickets(
		fullname, email, courseurl, class, 
		cname, program, degrees, page, week,
		issue, tier, submitteddate,
		user_notes, file1, file2, file3,
		priority, status, PVcodes
	) 
	VALUES(
		'".$fullname."', '".$email."', '".$url."', '".$classId."', 
		'".$className."', '".$program."', '".$degrees."', '".$page."', '".$week."', 
		'".$issue."', '".$tier."', '".$submitteddate."', 
		'".htmlspecialchars($note, ENT_QUOTES)."', '".$file1."', '".$file2."', '".$file3."', 
		'".$priority."', '".$status."', '".$pvcodes."'
	)";
	mysqli_query($conn, $statement) OR DIE('HAHA');
		
	mysqli_close($conn);
		
	$_SESSION['msg'] = "<div class='headingArea'>Added Ticket Successfully</div>";
}

if(isset($_POST['class_code'])) {
	$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$code = substr(str_shuffle($permitted_chars), 0, 10);
	include("../live_connect/connect.inc");

    $query = mysqli_query($conn, "SELECT * FROM tickets_newtickets ORDER BY id DESC LIMIT 3");
    $ci = $_POST['issue'];
    $cw = $_POST['week'];
    $cp = $_POST['page'];
    $cc = $_POST['class_code'];
	$name = $_POST['fname'];
	$email = $_POST['email'];

	$statement = "INSERT INTO tickets_usercodes (email, code) VALUES ('".$email."', '".$code."') 
	ON DUPLICATE KEY UPDATE email='".$email."', code = '".$code."'";
	mysqli_query($conn, $statement) OR DIE("HAHA");

    while($row = mysqli_fetch_array($query)) {
        $tnum = $row['id'];
        $tc = $row['class'];
        $tp = $row['page'];
        $tw = $row['week'];
		$ti = $row['issue'];
		$fname = $row['fullname'];
		$fname = explode(" ", $fname);
		$fname = $fname[1];
       

        if($tc == $cc && $tp == $cp && $tw == $cw && $ti == $ci) {
            $to = $email;
            $subject = "Your Ticket: #".$tnum;
            
            $msg = 
            "<html>
                <head>
                    <title>HTML email</title>
                </head>
                <body>
                    <h3> Hello Mr./Ms./Mrs. " . ucwords($fname) . ", </h3>
                    <p> Thank you for submitting a ticket to the IU CDT in an attempt to make our courses better. We have received your ticket
                    and will keep you updated on how things are going.</p>
                    <p>The current status of your ticket is <b style='color:red'>PENDING</b>. </p>
                    <p>You will receive additional emails as your ticket moves through our system. You have the ability to update your ticket how 
					you see fit while the ticket is still being worked on. Below is the link that you will need to use to modify your tickets. 
					<h3>For each
					additional ticket you submit, you will need to use the latest tickets initial email to access your editable tickets.</h3>
					<br />
					<br />
					<a href='https://www.iucdt.com/apps/tickets/editsubmittedtickets.php?email=$email&code=$code'>https://www.iucdt.com/apps/tickets/editsubmittedtickets.php</a></p>
                </body>
            </html>";

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= "From: no-reply@iucdt.com";


            mail($to, $subject, $msg, $headers);
        }
    }
}


?>
<body id='main_page'>
	<nav class='menu'>
		<?php include_once('includes/menu.php') ?>
	</nav>
	<content id='content' class='content'>
        <div class='headingArea'>Make A Ticket</div>
        <div class='page'>
        <b>Welcome to the curriculum development ticket system.</b><br />
Thank you for participating and helping to make these courses better.<p />
Please fill out and submit the ticket below to report any problems you have found.
<p />
All the fields must be filled in before submitting your ticket. 
<p />
<h3>All fields are required including a minimum of a single screenshot of the issue.</h3>
<form action='ticket.php' method='POST' enctype="multipart/form-data">
<table class='tab'>
<tr>
	<td></td>
	<td><font color='#A61D30'></font></td>
</tr>
<tr>
    <td class='tdh'>First Name:</td>
    <td class='tdc'>
        <input type='text' name='fname'/>
    </td>
    <td class='tdh'>Last Name:</td>
    <td class='tdc'>
        <input type='text' name='lname'/>
    </td>
</tr>
<tr>
    <td class='tdh'>E-mail:</td>
    <td class='email'>
        <input type='email' name='email'/>
    </td>
    <td  class='tdh'>Course URL:</td>
	<td ><input type='url' width='150px' name='curl' required/></td>
	
</tr>
<tr style="height: 10px;"></tr>
<tr>
    <td class='tdh' >Class:</td>
        <td class='tdc'>
            <select name='class_code' style="width:150px;" required>
                <option></option>
                <?php
                include("../live_connect/connect.inc");

                $query = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
                while($rows = mysqli_fetch_array($query)) {
                    $class = $rows['classId'];
                    echo "<option>" . $class . "</option>";
                }
                mysqli_close($conn);
                ?>
            </select>
	</td>
	<td class='tdh'>Page:</td>
	<td class='tdc'>
		<select name='page' style="width:150px;" required>
			<option></option>
			<option>Assessment</option>
			<option>Assignment</option>
			<option title='Any page in course that is not an assignment, assessment, discussion or daily checkpoint'>Content Page</option>
			<option>Daily Checkpoint</option>
			<option>Discussion</option>
		</select>
	</td>

</tr>
</tr>
<td class='tdh'>Week:</td>
	<td class='tdc'>
		<select name='week' style="width:150px;" required>
			<option></option>
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option title='Course is setup with topic or issue is in module other than week'>NA</option>
		</select>
	</td>
	<td class='tdh'>Issue:</td>
	<td class='tdc'>
		<select name='issue' style="width:150px;" required>
			<option></option>
			<?php
			include("../live_connect/connect.inc");

			$query = mysqli_query($conn, "SELECT issue, definition FROM tickets_issues ORDER BY issue ASC");
			while($rows = mysqli_fetch_array($query)) {
				echo "<option title='".$rows['definition']."'>" . $rows['issue'] . "</option>";
			}
			mysqli_close($conn);
			?>
		</select>
	</td>
	
</tr>
<tr>
	<td class='tdh'>Files:</td>
	<td colspan='3' >
		<input type="file" name="fileToUpload1" id="fileToUpload1" OnClick="fileCheck();">
	
		<input type="file" name="fileToUpload2" id="fileToUpload2">

		<input type="file" name="fileToUpload3" id="fileToUpload3">
	</td>
</tr>

<tr>
	<td class='tdh' colspan=4 style="text-align:left;"> Write the details of the issue here:</td>
</tr>
<tr>
	<td class='tdc' colspan=4>
		<textarea name='note' style='width:100%; height: 200px; resize:none;' required></textarea><br />
    </td>
</tr>


<tr>
	<td colspan='2' class='tdr'>
		<input type='submit' class='tdcbutton' value='Submit Ticket'>
	</td>
</tr>
</table>
</form>
        </div>
    </content>    
</body>
</html>

