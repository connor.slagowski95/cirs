<?php 
session_start([
    'cookie_lifetime' => 86400,
]); 
include("../includes/security.inc");
security("edit");
global $sdateadsort;
$editdate = date("Y-m-d");

if(isset($_POST['ustatus'])) {
	include("../../live_connect/connect.inc");
	$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE id = '".$_POST['uid']."'") OR DIE("HAHA");
	
	while($row = mysqli_fetch_array($query)) {
	
		if($_POST['ustatus'] != $row['status']) {
			$empname = $_COOKIE['un'];
			$empname = explode(".", $empname);
			$subname = $_POST['ufullname'];
			$subname = explode(".", $subname);
			$to = $_POST['uemail'];
			$tnum = $_POST['uid'];
			$subject = "Your Ticket: #".$tnum;
			$note = $_POST['ufinalnotes'];
			
			if ($_POST['ustatus'] == 'Assigned') {
				$msg = 
			"<html>
				<head>
					<title>HTML email</title>
				</head>
				<body>
				<h3> Hello Mr./Ms./Mrs. " . ucwords($fname[1]) . ", </h3>
				<p>My name is ".ucwords($empname[0])." and I have taken on your ticket that you submitted to the CDT. </p>
				<p>The current status of your ticket is <b style='color:#dbcf1f'>ASSIGNED</b>. </p>
				<p>You will receive additional emails as your ticket moves through our system.</p>
				</body>
			</html>";
			}
			if ($_POST['ustatus'] == 'Pending') {
	
				$msg = 
			"<html>
				<head>
					<title>HTML email</title>
				</head>
				<body>
					<h3> Hello Mr. /Ms. /Mrs. " . ucwords($subname[1]) . ", </h3>
					<p>Your ticket has been set back to the <b style='color:orange'>".strtoupper($_POST['ustatus'])."</b> status. </p>
	
					<p>It will be reassigned to a new CDT member when we get the chance to get back to it. Thank you for your patience, in the mean time.</p>
					<p>You will receive additional emails as your ticket moves through our system.</p>
				</body>
			</html>";
			} 
			
			if ($_POST['ustatus'] == 'Completed') {
	
				$msg = 
			"<html>
				<head>
					<title>HTML email</title>
				</head>
				<body>
					<h3> Hello Mr. /Ms. /Mrs. " . ucwords($subname[1]) . ", </h3>
					<p>We thank you for your submission to the CDT and we are happy to say that your ticket number ".$tnum." has been taken care
					of. </p>
					<p>The current status of your ticket is <b style='color:green'>".strtoupper($_POST['ustatus'])."
					</b></p>
					<p>Here are the final notes pertaining to the issue: </p>
							<p><b>" . $note . "</b></p>
					<p>We hope that you have a wonderful day.</p>
				</body>
			</html>";
			}
			if ($_POST['ustatus'] == 'Rejected') {
	
				$msg = 
			"<html>
				<head>
					<title>HTML email</title>
				</head>
				<body>
					<h3> Hello Mr. /Ms. /Mrs. " . ucwords($subname[1]) . ", </h3>
					<p>We have reviewed the ticket that you submitted to the CDT and your request has been denied. The reasoning for this
					decision is recorded below for your benefit.</p>
					<p>The current status of your ticket is <b style='color:red'>".strtoupper($_POST['ustatus'])."
					</b></p>
					
					<p>The reason for the rejection: <b>".$note."</b></p>
					
				</body>
			</html>";
			}
			if ($_POST['ustatus'] == 'Hold') {
	
				$msg = 
			"<html>
				<head>
					<title>HTML email</title>
				</head>
				<body>
					<h3> Hello Mr. /Ms. /Mrs. " . ucwords($subname[1]) . ", </h3>
					<p>The status on your ticket has been update.</p>
					<p>The current status of your ticket is <b style='color:blue'>".strtoupper($_POST['ustatus'])."
					</b></p>
					<p>This update indicates that your ticket has not been denied but may take a bit more time to complete. 
					We thank you for your patience in the mean time.</p>
				</body>
			</html>";
			}
	
			
	
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= "From: no-reply@iucdt.com";
	
	
			mail($to, $subject, $msg, $headers);
		}
	}
	}

if(isset($_POST['uid'])) {
	$fullname = $_POST['ufullname'];
	$email = $_POST['uemail'];
	$courseurl = $_POST['ucourseurl'];
	$class = $_POST['uclass'];
	$program = $_POST['uprogram'];
	$page = $_POST['upage'];
	$week = $_POST['uweek'];
	$issue = $_POST['uissue'];
	$tier = $_POST['utier'];
	$usernotes = htmlspecialchars($_POST['uusernotes'], ENT_QUOTES);
	$cdtnotes = htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES);
	$finalnotes = htmlspecialchars($_POST['ufinalnotes'], ENT_QUOTES);
	$priority = $_POST['upriority'];
	$appliedto = $_POST['uapplied'];
	$authorizedby = $_POST['uauth'];
	$agent = $_POST['uagent'];
	$status = $_POST['ustatus'];
	$datetaken = date("Y-m-d");
	if($status == 'Rejected' || $status == 'Completed') {
		$completeddate = date("Y-m-d");
	}
	elseif($status != 'Rejected' && $status != 'Completed') {
		$completeddate = "0000-00-00";
	}
		
	include("../../live_connect/connect.inc");
	
	$query = mysqli_query($conn, "SELECT agent FROM tickets_newtickets WHERE id = '".$_POST['uid']."'");
	while($rows = mysqli_fetch_array($query)) {
		$oldagent = $rows['agent'];
	}
	$query = mysqli_query($conn, "SELECT className FROM tickets_newclasses WHERE classId = '".$class."'");
	while($rows = mysqli_fetch_array($query)) {
		$cname = $rows['className'];
	}
	
	if($oldagent != $agent) {
		$status = 'Assigned';
	}
	
	$q = "UPDATE tickets_newtickets SET 
	fullname = '".$fullname."', 
	email = '".$email."', 
	courseurl = '".$courseurl."', 
	class = '".$class."',
	cname = '".$cname."',
	program = '".$program."',
	page = '".$page."', 
	week = '".$week."', 
	issue = '".$issue."', 
	tier = '".$tier."', 
	user_notes = '".$usernotes."',
	CDT_notes = '".$cdtnotes."',
	final_notes = '".$finalnotes."',
	priority = '".$priority."', 
	appliedto = '".$appliedto."',
	authorizedby = '".$authorizedby."',
	agent = '".$agent."', 
	status = '".$status."', 
	completeddate = '".$completeddate."'
	WHERE id = '".$_POST['uid']."'";
	mysqli_query($conn, $q) OR DIE("HAHA");
	
	mysqli_query($conn, "INSERT INTO tickets_newticketlog(
		ticketid, 
		usertaken, 
		datetaken,
		status, 
		notes, 
		datecompleted,
		editdate
		) 
		VALUES(
		'".$_POST['uid']."', 
		'".$agent."', 
		'".$datetaken."',
		'".$status."', 
		'".$notes."', 
		'".$completeddate."', 
		'".$editdate."')");
		
	mysqli_close($conn);
	$_SESSION['msg'] = "<div class='headingArea'>Updated Note Successfully</div>";
}


?>

<style>
	table {
		border: 1px solid lightgrey;
	}
	.tdh {
		text-align:left;
		color: #FFFFFF;
		background-color: #013245;
		border: none;
		padding-left: 5px;
	}
	.tdc {
		border: solid lightgray;
		border-width: 1px 1px 0px 0px;
		padding: 5px 5px;
	}
	.tdcbutton {
		padding: 5px;
		font-weight: bold;
	}
	#form {
		grid-row: 2/3;
		grid-column: 1/-1;
	}
</style>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page title</title>
    
    <link rel="stylesheet" href="../includes/styles.css">

    <style>
        .tab {
			border-collapse: collapse;
			width: 100%;
		}
    </style>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script>
		$(document).ready(function (){
			$("#cont").css("display", "none");
			$("#hide").click(function (){
				$("#cont").css("display", "none");
			});
			$("#show").click(function (){
				$("#cont").css("display", "initial");
			});
		});
	</script>
</head>
<body id='main_page'>
    <nav class='menu'>
            <?php include_once('../includes/adminmenu.php') ?>
        </nav>
<content class="content" id='content'>


<div class="headingArea">
	Supervisor Tickets
</div>

<div class='page'>
	<table style='border: none;' class='tab'> 
		<tr>
			<td colspan='3'>
				Sorting Feature
			</td>
			<td colspan='3'>
				Status Search Feature
			</td>
            <td colspan = '2'>
				Ticket ID
			</td>
			
		</tr>
		<form action='suptickets.php' method='POST'>
			<td>
				<select name='colsort'>
					<?php echo "
						<option value='id' "; if(isset($_POST['colsort']) && $_POST['colsort']=='id') { echo " selected"; } echo ">ID</option>
						<option value='fullname' "; if(isset($_POST['colsort']) && $_POST['colsort']=='fullname') { echo " selected"; } echo ">Fullname</option>
						<option value='email' "; if(isset($_POST['colsort']) && $_POST['colsort']=='email') { echo " selected"; } echo ">E-Mail</option>
						<option value='courseurl' "; if(isset($_POST['colsort']) && $_POST['colsort']=='courseurl') { echo " selected"; } echo ">Course URL</option>
						<option value='class' "; if(isset($_POST['colsort']) && $_POST['colsort']=='class') { echo " selected"; } echo ">Class</option>
						<option value='page' "; if(isset($_POST['colsort']) && $_POST['colsort']=='page') { echo " selected"; } echo ">Page</option>
						<option value='week' "; if(isset($_POST['colsort']) && $_POST['colsort']=='week') { echo " selected"; } echo ">Week</option>
						<option value='reason' "; if(isset($_POST['colsort']) && $_POST['colsort']=='reason') { echo " selected"; } echo ">Reason</option>
						<option value='tier' "; if(isset($_POST['colsort']) && $_POST['colsort']=='tier') { echo " selected"; } echo ">Tier</option>
						<option value='priority' "; if(isset($_POST['colsort']) && $_POST['colsort']=='priority') { echo " selected"; } echo ">Priority</option>
						<option value='agent' "; if(isset($_POST['colsort']) && $_POST['colsort']=='agent') { echo " selected"; } echo ">Agent</option>
						<option value='submitteddate' "; if(isset($_POST['colsort']) && $_POST['colsort']=='submitteddate') { echo " selected"; } echo ">Submitted Date</option>
						<option value='status' "; if(isset($_POST['colsort']) && $_POST['colsort']=='status') { echo " selected"; } echo ">Status</option>
					"; ?>
				</select>
			</td>
			<td>
				<select name='adsort' style='width:100%;'>
					<?php echo "
						<option value='ASC' "; if(isset($_POST['adsort']) && $_POST['adsort']=='ASC') { echo " selected"; } echo ">Ascending</option>
						<option value='DESC' "; if(isset($_POST['adsort']) && $_POST['adsort']=='DESC') { echo " selected"; } echo ">Descending</option>
					"; ?>
				</select>
			</td>
			<td>
				<input type='submit' value='Search' class='search'>
			</td>
		</form>
		<form action='suptickets.php' method='POST'>
			<td>
				<select name='searchstatus'>
					<?php echo "
						<option value='Assigned' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Assigned') { echo " selected"; } echo ">Assigned</option>
						<option value='Completed' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Completed') { echo " selected"; } echo ">Completed</option>
						<option value='Pending' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Pending') { echo " selected"; } echo ">Pending</option>
						<option value='Rejected' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Rejected') { echo " selected"; } echo ">Rejected</option>	
					"; ?>
				</select>
			</td>
			<td>
				<select name='searchstatusad' style='width:100%;'>
					<?php echo "
						<option value='ASC' "; if(isset($_POST['searchstatusad']) && $_POST['searchstatusad']=='ASC') { echo " selected"; } echo ">Ascending</option>
						<option value='DESC' "; if(isset($_POST['searchstatusad']) && $_POST['searchstatusad']=='DESC') { echo " selected"; } echo ">Descending</option>
					"; ?>
				</select>
			</td>
			<td>
				<input type='submit' value='Search' class='search'>
			</td>
		</form>
        <form action='suptickets.php' method='POST'>
			<td>
				<input type='text' name='searchid' value='<?php if(isset($_POST['searchid'])) echo $_POST['searchid']; ?>'>
			</td>
			<td>
				<input type='submit' value='Search' class='search'>
			</td>
		</form>
		
		</tr>
		<tr>
			<td colspan = '4'>
				Date Range Search All Records
			</td>
			<td colspan='4'>
				Agent Search
			</td>
		</tr>
		<tr>
		<form action='suptickets.php' method='POST'>
			<td>
				<input type='date' name='sdate' value='<?php if(isset($_POST['sdate'])) echo $_POST['sdate']; ?>'>
			</td>
			<td>
				<input type='date' name='edate' value='<?php if(isset($_POST['edate'])) echo $_POST['edate']; ?>'>
			</td>
			<td>
				<select name='sdateadsort'>
				<?php
					echo "
					<option value='ASC' "; if(isset($_POST['sdateadsort']) && $_POST['sdateadsort']=='ASC') { echo " selected"; } echo ">Ascending</option>
					<option value='DESC' "; if(isset($_POST['sdateadsort']) && $_POST['sdateadsort']=='DESC') { echo " selected"; } echo ">Descending</option>
				"; ?>
				</select>
			</td>
			<td>
				<input type='submit' value='Search' class='search'>
			</td>
		</form>
		<form action='suptickets.php' method='POST'>
			<td>
				<select name='sagent'>
					<?php
						include("../../live_connect/connect.inc");
						$qsa = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
						while($rsa = mysqli_fetch_array($qsa)) {
							$agent = $rsa['username'];
							$agent = explode('.', $agent);
							$agent = implode(" ", $agent);
							$agent = ucwords($agent);
							echo "<option value='".$rsa['username']."' "; if(isset($_POST['sagent']) && $rsa['username'] == $_POST['sagent']) { echo " selected"; } echo ">".$agent."</option>";
						}
						mysqli_close($conn);
					echo "
				</select>
			</td>
			<td>
				<select name='sstatus'>
					<option value='Assigned' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Assigned') { echo " selected"; } echo ">Assigned</option>
					<option value='Completed' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Completed') { echo " selected"; } echo ">Completed</option>
					<option value='Pending' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Pending') { echo " selected"; } echo ">Pending</option>
					<option value='Rejected' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Rejected') { echo " selected"; } echo ">Rejected</option>
					"; ?>
				</select>
			</td>
			<td>
				<select name='sadsort'>
					<?php echo "
					<option value='ASC' "; if(isset($_POST['sadsort']) && $_POST['sadsort']=='ASC') { echo " selected"; } echo ">Ascending</option>
					<option value='DESC' "; if(isset($_POST['sadsort']) && $_POST['sadsort']=='DESC') { echo " selected"; } echo ">Descending</option>
					"; ?>
				</select>
			</td>
			<td>
				<input type='submit' value='Search' class='search'>
			</td>
		</form>
		</tr>
	</table>
</div>
<div class='page'>
<?php
if(isset($_POST['sdateadsort'])) {
	$sdate = $_POST['sdate'];
	$edate = $_POST['edate'];
	$sdateadsort = $_POST['sdateadsort'];
	$statement = "SELECT * FROM tickets_newtickets WHERE submitteddate >= '".$sdate."' AND submitteddate <= '".$edate."' ORDER BY id ".$sdateadsort;
	echo "<table>";
	include("../../live_connect/connect.inc");
	
$query = mysqli_query($conn, $statement) OR DIE("HAHA");
while($rows = mysqli_fetch_array($query)) {
	echo "<form action='suptickets.php' method='POST'>
	<tr>
		<td class='tdh'>ID</td>
		<td class='tdh'>Fullname</td>
		<td class='tdh'>E-Mail</td>
		<td class='tdh' colspan='2'>Course URL</td>
		<td class='tdh'>Class</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>Page</td>
	</tr>
	<tr>
			<td class='tdc'>" . $rows['id'] . "<input type='hidden' name='uid' value='".$rows['id']."'></td>
			<td class='tdc'>" . $rows['fullname'] . "<input type='hidden' name='ufullname' value='".$rows['fullname']."'></td>
			<td class='tdc'>" . $rows['email'] . "<input type='hidden' name='uemail' value='".$rows['email']."'></td>
			<td class='tdc' colspan='2'><input type='url' title='Please make sure you use https:// or http:// with your URL' name='ucourseurl' value='".$rows['courseurl']."'>
		<a href='".$rows['courseurl']."' target='_blank'>[URL]</a></td>
			<td class='tdc'>
				<select name='uclass'>";
					$q = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
					while($r = mysqli_fetch_array($q)) {
						echo "<option "; if ($r['classId'] == $rows['class']) { echo " selected"; } echo ">".$r['classId']."</option>";
					}
				echo "</select>
			</td>
			<td class='tdc'>".$rows['cname']."</td>
			<td class='tdc'>
				<select name='upage'>";
					echo "<option "; if ($rows['page'] == '') { echo " selected"; } echo "></option>";
					echo "<option "; if ($rows['page'] == 'Assessment') { echo " selected"; } echo ">Assessment</option>";
					echo "<option "; if ($rows['page'] == 'Assignment') { echo " selected"; } echo ">Assignment</option>";
					echo "<option title='Any page in course that is not an assignment, assessment, discussion or daily checkpoint' "; if ($rows['page'] == 'Content Page') { echo " selected"; } echo ">Content Page</option>";
					echo "<option "; if ($rows['page'] == 'Daily Checkpoint') { echo " selected"; } echo ">Daily Checkpoint</option>";
					echo "<option "; if ($rows['page'] == 'Discussion') { echo " selected"; } echo ">Discussion</option>";
				echo "</select>
			</td>
	</tr>
	<tr>
		<td class='tdh'>Program</td>
		<td class='tdh'>Week</td>
		<td class='tdh'>Issue</td>
		<td class='tdh' colspan='2'>Tier</td>
		<td class='tdh'>Priority</td>
		<td class='tdh'>Submitted Date</td>
		<td class='tdh'>Status</td>
	</tr>
	<tr>
		<td class='tdc'>
			<select name='uprogram'>
				<option></option>";
						$query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms") OR DIE("HAHA");

						while($row2 = mysqli_fetch_array($query2)) {
							echo "<option value='".$row2['program']."'"; if($rows['program'] == $row2['program']) {echo 'selected';} echo "> ".$row2['program']."</option>";
						}
				echo "
			</select>
		</td>
		<td class='tdc'>
			<select name='uweek' style='width:100%'>";
				echo "<option "; if ($rows['week'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['week'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['week'] == '3') { echo " selected"; } echo ">3</option>";
				echo "<option "; if ($rows['week'] == '4') { echo " selected"; } echo ">4</option>";
				echo "<option "; if ($rows['week'] == 'NA') { echo " selected"; } echo ">NA</option>";
			echo "</select>
		</td>
		</td>
		<td class='tdc'>
			<select name='uissue' style='width:100%'>";
				$q = mysqli_query($conn, "SELECT issue FROM tickets_issues ORDER BY issue ASC");
				while($r = mysqli_fetch_array($q)) {
					echo "<option "; if ($r['issue'] == $rows['issue']) { echo " selected"; } echo ">".$r['issue']."</option>";
				}
			echo "</select>
		</td>
		<td class='tdc'  colspan='2'>
			<select name='utier' style='width:100%'>";
				echo "<option "; if ($rows['tier'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['tier'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['tier'] == '3') { echo " selected"; } echo ">3</option>";
				
			echo "</select>
		</td>
		<td class='tdc'>
			<select name='upriority' style='width:100%'>";
				echo "<option value=''"; if ($rows['priority'] == '') { echo " selected"; } echo "></option>";
				echo "<option value='Low'"; if ($rows['priority'] == 'Low') { echo " selected"; } echo ">Low</option>";
				echo "<option value='Medium'"; if ($rows['priority'] == 'Medium') { echo " selected"; } echo ">Medium</option>";
				echo "<option value='High'"; if ($rows['priority'] == 'High') { echo " selected"; } echo ">High</option>";
			echo "</select>
		</td>
		<td class='tdc'>" . $rows['submitteddate'] . "<input type='hidden' name='usubmitteddate' value='".$rows['submitteddate']."'></td>
		<td class='tdc'>
			<select name='ustatus' style='width:100%'>
				<option value='Assigned' "; if($rows['status']=='Assigned') { echo " selected"; } echo ">Assigned</option>
				<option value='Completed' "; if($rows['status']=='Completed') { echo " selected"; } echo ">Completed</option>
				<option value='Pending' "; if($rows['status']=='Pending') { echo " selected"; } echo ">Pending</option>
				<option value='Rejected' "; if($rows['status']=='Rejected') { echo " selected"; } echo ">Rejected</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class='tdh'>Apply Change To</td>
		<td class='tdh'>Authorized by</td>
		<td class='tdh'>File 1</td>
		<td class='tdh'>File 2</td>
		<td class='tdh'>File 3</td>
		<td class='tdh'>Assigned To</td>
		<td class='tdh'>Assigned Date</td>
		<td class='tdh'>Due Date</td>
	</tr>
	<tr>
		<td class='tdc'><select name='uapplied' style='width: 100%;'>
			<option></option>
			<option"; if($rows['appliedto'] == 'Online') { echo " selected"; } echo ">Online</option>
			<option"; if($rows['appliedto'] == 'Blended') { echo " selected"; } echo ">Blended</option>
			<option"; if($rows['appliedto'] == 'Both') { echo " selected"; } echo ">Both</option>
			</select>
		</td>
		<td class='tdc'>
				<select name='uauth'>
					<option></option>";
					$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
					while($r = mysqli_fetch_array($q)) {
						$user = explode(".", $r['username']);
						$user = implode(" ", $user);
						$user = ucwords($user);
						echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['authorizedby']) { echo " selected"; } echo ">".$user."</option>";
					}
				echo "</select></td>
		<td class='tdc'>"; if($rows['file1'] != '') echo "<a href='../uploads/".$rows['file1']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file2'] != '') echo "<a href='../uploads/".$rows['file2']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file3'] != '') echo "<a href='../uploads/".$rows['file3']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>
			<select name='uagent'>
				<option></option>";
				$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
				while($r = mysqli_fetch_array($q)) {
					$user = explode(".", $r['username']);
					$user = implode(" ", $user);
					$user = ucwords($user);
					echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['agent']) { echo " selected"; } echo ">".$user."</option>";
				}
			echo "</select></td>";
		echo "<td class='tdc'>".$rows['assigneddate']."</td>";
		echo "<td class='tdc'>".$rows['duedate']."</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>User Notes</td>
		<td class='tdh' colspan='3'>CDT Notes</td>
		<td class='tdh' colspan='3'>Final Notes</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2' valign='top'>" . $rows['user_notes'] . " <input type='hidden' name='uusernotes' value='".$rows['user_notes']."'/></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ucdtnotes'>".$rows['CDT_notes']."</textarea></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ufinalnotes'>" . $rows['final_notes'] . "</textarea></td>
	</tr>
	<tr style='background-color: #ccc;'>
		<td class='tdc' colspan='12' style='text-align: left;'>
			<input class='tdcbutton' type='submit' value='Update Ticket' style='float: right;'></form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
            <a href='#show' id='show'>[Show]</a>
            <a href='#hide' id='hide'>/ [Hide]</a>
			<span id='cont' style='display: none;'>"; 
			$query2 = mysqli_query($conn, "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
			l.status AS status,	l.notes AS notes, t.duedate AS duedate, l.editdate AS editdate 
			FROM tickets_newtickets t, tickets_newticketlog l 
			WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'");
		echo "<table class='tab2' style='width:99%;'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $r2['datetaken'] . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $r2['duedate'] . "</td>
				<td class='tdc'>" . $r2['editdate'] . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>CDT Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			";  
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	";
}
mysqli_close($conn);
echo "</table>";
}	
elseif(isset($_POST['searchstatus'])) {
	$searchstatus = $_POST['searchstatus'];
	$searchstatusad = $_POST['searchstatusad'];
	echo "<table class='tab'>";
	include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE status = '".$searchstatus."' ORDER BY submitteddate ".$searchstatusad."");
while($rows = mysqli_fetch_array($query)) {
	echo "<form action='suptickets.php' method='POST'>
	<tr>
		<td class='tdh'>ID</td>
		<td class='tdh'>Fullname</td>
		<td class='tdh'>E-Mail</td>
		<td class='tdh' colspan='2'>Course URL</td>
		<td class='tdh'>Class</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>Page</td>
	</tr>
	<tr>
			<td class='tdc'>" . $rows['id'] . "<input type='hidden' name='uid' value='".$rows['id']."'></td>
			<td class='tdc'>" . $rows['fullname'] . "<input type='hidden' name='ufullname' value='".$rows['fullname']."'></td>
			<td class='tdc'>" . $rows['email'] . "<input type='hidden' name='uemail' value='".$rows['email']."'></td>
			<td class='tdc' colspan='2'><input type='url' title='Please make sure you use https:// or http:// with your URL' name='ucourseurl' value='".$rows['courseurl']."'>
		<a href='".$rows['courseurl']."' target='_blank'>[URL]</a></td>
			<td class='tdc'>
				<select name='uclass'>";
					$q = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
					while($r = mysqli_fetch_array($q)) {
						echo "<option "; if ($r['classId'] == $rows['class']) { echo " selected"; } echo ">".$r['classId']."</option>";
					}
				echo "</select>
			</td>
			<td class='tdc'>".$rows['cname']."</td>
			<td class='tdc'>
				<select name='upage'>";
					echo "<option "; if ($rows['page'] == '') { echo " selected"; } echo "></option>";
					echo "<option "; if ($rows['page'] == 'Assessment') { echo " selected"; } echo ">Assessment</option>";
					echo "<option "; if ($rows['page'] == 'Assignment') { echo " selected"; } echo ">Assignment</option>";
					echo "<option title='Any page in course that is not an assignment, assessment, discussion or daily checkpoint' "; if ($rows['page'] == 'Content Page') { echo " selected"; } echo ">Content Page</option>";
					echo "<option "; if ($rows['page'] == 'Daily Checkpoint') { echo " selected"; } echo ">Daily Checkpoint</option>";
					echo "<option "; if ($rows['page'] == 'Discussion') { echo " selected"; } echo ">Discussion</option>";
				echo "</select>
			</td>
	</tr>
	<tr>
		<td class='tdh'>Program</td>
		<td class='tdh'>Week</td>
		<td class='tdh'>Issue</td>
		<td class='tdh' colspan='2'>Tier</td>
		<td class='tdh'>Priority</td>
		<td class='tdh'>Submitted Date</td>
		<td class='tdh'>Status</td>
	</tr>
	<tr>
		<td class='tdc'>
			<select name='uprogram'>
				<option></option>";
						$query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms") OR DIE("HAHA");

						while($row2 = mysqli_fetch_array($query2)) {
							echo "<option value='".$row2['program']."'"; if($rows['program'] == $row2['program']) {echo 'selected';} echo "> ".$row2['program']."</option>";
						}
				echo "
			</select>
		</td>
		<td class='tdc'>
			<select name='uweek' style='width:100%'>";
				echo "<option "; if ($rows['week'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['week'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['week'] == '3') { echo " selected"; } echo ">3</option>";
				echo "<option "; if ($rows['week'] == '4') { echo " selected"; } echo ">4</option>";
				echo "<option "; if ($rows['week'] == 'NA') { echo " selected"; } echo ">NA</option>";
			echo "</select>
		</td>
		</td>
		<td class='tdc'>
			<select name='uissue' style='width:100%'>";
				$q = mysqli_query($conn, "SELECT issue FROM tickets_issues ORDER BY issue ASC");
				while($r = mysqli_fetch_array($q)) {
					echo "<option "; if ($r['issue'] == $rows['issue']) { echo " selected"; } echo ">".$r['issue']."</option>";
				}
			echo "</select>
		</td>
		<td class='tdc'  colspan='2'>
			<select name='utier' style='width:100%'>";
				echo "<option "; if ($rows['tier'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['tier'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['tier'] == '3') { echo " selected"; } echo ">3</option>";
				
			echo "</select>
		</td>
		<td class='tdc'>
			<select name='upriority' style='width:100%'>";
				echo "<option value=''"; if ($rows['priority'] == '') { echo " selected"; } echo "></option>";
				echo "<option value='Low'"; if ($rows['priority'] == 'Low') { echo " selected"; } echo ">Low</option>";
				echo "<option value='Medium'"; if ($rows['priority'] == 'Medium') { echo " selected"; } echo ">Medium</option>";
				echo "<option value='High'"; if ($rows['priority'] == 'High') { echo " selected"; } echo ">High</option>";
			echo "</select>
		</td>
		<td class='tdc'>" . $rows['submitteddate'] . "<input type='hidden' name='usubmitteddate' value='".$rows['submitteddate']."'></td>
		<td class='tdc'>
			<select name='ustatus' style='width:100%'>
				<option value='Assigned' "; if($rows['status']=='Assigned') { echo " selected"; } echo ">Assigned</option>
				<option value='Completed' "; if($rows['status']=='Completed') { echo " selected"; } echo ">Completed</option>
				<option value='Pending' "; if($rows['status']=='Pending') { echo " selected"; } echo ">Pending</option>
				<option value='Rejected' "; if($rows['status']=='Rejected') { echo " selected"; } echo ">Rejected</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class='tdh'>Apply Change To</td>
		<td class='tdh'>Authorized by</td>
		<td class='tdh'>File 1</td>
		<td class='tdh'>File 2</td>
		<td class='tdh'>File 3</td>
		<td class='tdh'>Assigned To</td>
		<td class='tdh'>Assigned Date</td>
		<td class='tdh'>Due Date</td>
	</tr>
	<tr>
		<td class='tdc'><select name='uapplied' style='width: 100%;'>
			<option></option>
			<option"; if($rows['appliedto'] == 'Online') { echo " selected"; } echo ">Online</option>
			<option"; if($rows['appliedto'] == 'Blended') { echo " selected"; } echo ">Blended</option>
			<option"; if($rows['appliedto'] == 'Both') { echo " selected"; } echo ">Both</option>
			</select>
		</td>
		<td class='tdc'>
				<select name='uauth'>
					<option></option>";
					$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
					while($r = mysqli_fetch_array($q)) {
						$user = explode(".", $r['username']);
						$user = implode(" ", $user);
						$user = ucwords($user);
						echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['authorizedby']) { echo " selected"; } echo ">".$user."</option>";
					}
				echo "</select></td>
		<td class='tdc'>"; if($rows['file1'] != '') echo "<a href='../uploads/".$rows['file1']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file2'] != '') echo "<a href='../uploads/".$rows['file2']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file3'] != '') echo "<a href='../uploads/".$rows['file3']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>
			<select name='uagent'>
				<option></option>";
				$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
				while($r = mysqli_fetch_array($q)) {
					$user = explode(".", $r['username']);
					$user = implode(" ", $user);
					$user = ucwords($user);
					echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['agent']) { echo " selected"; } echo ">".$user."</option>";
				}
			echo "</select></td>";
		echo "<td class='tdc'>".$rows['assigneddate']."</td>";
		echo "<td class='tdc'>".$rows['duedate']."</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>User Notes</td>
		<td class='tdh' colspan='3'>CDT Notes</td>
		<td class='tdh' colspan='3'>Final Notes</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2' valign='top'>" . $rows['user_notes'] . " <input type='hidden' name='uusernotes' value='".$rows['user_notes']."'/></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ucdtnotes'>".$rows['CDT_notes']."</textarea></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ufinalnotes'>" . $rows['final_notes'] . "</textarea></td>
	</tr>
	<tr style='background-color: #ccc;'>
		<td class='tdc' colspan='12' style='text-align: left;'>
			<input class='tdcbutton' type='submit' value='Update Ticket' style='float: right;'></form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
            <a href='#show' id='show'>[Show]</a>
            <a href='#hide' id='hide'>/ [Hide]</a>
			<span id='cont' style='display: none;'>"; 
			$query2 = mysqli_query($conn, "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
			l.status AS status,	l.notes AS notes, t.duedate AS duedate, l.editdate AS editdate 
			FROM tickets_newtickets t, tickets_newticketlog l 
			WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'");
		echo "<table class='tab2' style='width:99%;'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $r2['datetaken'] . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $r2['duedate'] . "</td>
				<td class='tdc'>" . $r2['editdate'] . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>CDT Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			";  
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	";
}
mysqli_close($conn);
echo "</table>";
}elseif(isset($_POST['searchid'])) {
	$searchid = $_POST['searchid'];
	echo "<table class='tab'>";
	include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE id = '".$searchid."' ORDER BY submitteddate");
while($rows = mysqli_fetch_array($query)) {
	echo "<form action='suptickets.php' method='POST'>
	<tr>
		<td class='tdh'>ID</td>
		<td class='tdh'>Fullname</td>
		<td class='tdh'>E-Mail</td>
		<td class='tdh' colspan='2'>Course URL</td>
		<td class='tdh'>Class</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>Page</td>
	</tr>
	<tr>
			<td class='tdc'>" . $rows['id'] . "<input type='hidden' name='uid' value='".$rows['id']."'></td>
			<td class='tdc'>" . $rows['fullname'] . "<input type='hidden' name='ufullname' value='".$rows['fullname']."'></td>
			<td class='tdc'>" . $rows['email'] . "<input type='hidden' name='uemail' value='".$rows['email']."'></td>
			<td class='tdc' colspan='2'><input type='url' title='Please make sure you use https:// or http:// with your URL' name='ucourseurl' value='".$rows['courseurl']."'>
		<a href='".$rows['courseurl']."' target='_blank'>[URL]</a></td>
			<td class='tdc'>
				<select name='uclass'>";
					$q = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
					while($r = mysqli_fetch_array($q)) {
						echo "<option "; if ($r['classId'] == $rows['class']) { echo " selected"; } echo ">".$r['classId']."</option>";
					}
				echo "</select>
			</td>
			<td class='tdc'>".$rows['cname']."</td>
			<td class='tdc'>
				<select name='upage'>";
					echo "<option "; if ($rows['page'] == '') { echo " selected"; } echo "></option>";
					echo "<option "; if ($rows['page'] == 'Assessment') { echo " selected"; } echo ">Assessment</option>";
					echo "<option "; if ($rows['page'] == 'Assignment') { echo " selected"; } echo ">Assignment</option>";
					echo "<option title='Any page in course that is not an assignment, assessment, discussion or daily checkpoint' "; if ($rows['page'] == 'Content Page') { echo " selected"; } echo ">Content Page</option>";
					echo "<option "; if ($rows['page'] == 'Daily Checkpoint') { echo " selected"; } echo ">Daily Checkpoint</option>";
					echo "<option "; if ($rows['page'] == 'Discussion') { echo " selected"; } echo ">Discussion</option>";
				echo "</select>
			</td>
	</tr>
	<tr>
		<td class='tdh'>Program</td>
		<td class='tdh'>Week</td>
		<td class='tdh'>Issue</td>
		<td class='tdh' colspan='2'>Tier</td>
		<td class='tdh'>Priority</td>
		<td class='tdh'>Submitted Date</td>
		<td class='tdh'>Status</td>
	</tr>
	<tr>
		<td class='tdc'>
			<select name='uprogram'>
				<option></option>";
						$query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms") OR DIE("HAHA");

						while($row2 = mysqli_fetch_array($query2)) {
							echo "<option value='".$row2['program']."'"; if($rows['program'] == $row2['program']) {echo 'selected';} echo "> ".$row2['program']."</option>";
						}
				echo "
			</select>
		</td>
		<td class='tdc'>
			<select name='uweek' style='width:100%'>";
				echo "<option "; if ($rows['week'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['week'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['week'] == '3') { echo " selected"; } echo ">3</option>";
				echo "<option "; if ($rows['week'] == '4') { echo " selected"; } echo ">4</option>";
				echo "<option "; if ($rows['week'] == 'NA') { echo " selected"; } echo ">NA</option>";
			echo "</select>
		</td>
		</td>
		<td class='tdc'>
			<select name='uissue' style='width:100%'>";
				$q = mysqli_query($conn, "SELECT issue FROM tickets_issues ORDER BY issue ASC");
				while($r = mysqli_fetch_array($q)) {
					echo "<option "; if ($r['issue'] == $rows['issue']) { echo " selected"; } echo ">".$r['issue']."</option>";
				}
			echo "</select>
		</td>
		<td class='tdc'  colspan='2'>
			<select name='utier' style='width:100%'>";
				echo "<option "; if ($rows['tier'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['tier'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['tier'] == '3') { echo " selected"; } echo ">3</option>";
				
			echo "</select>
		</td>
		<td class='tdc'>
			<select name='upriority' style='width:100%'>";
				echo "<option value=''"; if ($rows['priority'] == '') { echo " selected"; } echo "></option>";
				echo "<option value='Low'"; if ($rows['priority'] == 'Low') { echo " selected"; } echo ">Low</option>";
				echo "<option value='Medium'"; if ($rows['priority'] == 'Medium') { echo " selected"; } echo ">Medium</option>";
				echo "<option value='High'"; if ($rows['priority'] == 'High') { echo " selected"; } echo ">High</option>";
			echo "</select>
		</td>
		<td class='tdc'>" . $rows['submitteddate'] . "<input type='hidden' name='usubmitteddate' value='".$rows['submitteddate']."'></td>
		<td class='tdc'>
			<select name='ustatus' style='width:100%'>
				<option value='Assigned' "; if($rows['status']=='Assigned') { echo " selected"; } echo ">Assigned</option>
				<option value='Completed' "; if($rows['status']=='Completed') { echo " selected"; } echo ">Completed</option>
				<option value='Pending' "; if($rows['status']=='Pending') { echo " selected"; } echo ">Pending</option>
				<option value='Rejected' "; if($rows['status']=='Rejected') { echo " selected"; } echo ">Rejected</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class='tdh'>Apply Change To</td>
		<td class='tdh'>Authorized by</td>
		<td class='tdh'>File 1</td>
		<td class='tdh'>File 2</td>
		<td class='tdh'>File 3</td>
		<td class='tdh'>Assigned To</td>
		<td class='tdh'>Assigned Date</td>
		<td class='tdh'>Due Date</td>
	</tr>
	<tr>
		<td class='tdc'><select name='uapplied' style='width: 100%;'>
			<option></option>
			<option"; if($rows['appliedto'] == 'Online') { echo " selected"; } echo ">Online</option>
			<option"; if($rows['appliedto'] == 'Blended') { echo " selected"; } echo ">Blended</option>
			<option"; if($rows['appliedto'] == 'Both') { echo " selected"; } echo ">Both</option>
			</select>
		</td>
		<td class='tdc'>
				<select name='uauth'>
					<option></option>";
					$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
					while($r = mysqli_fetch_array($q)) {
						$user = explode(".", $r['username']);
						$user = implode(" ", $user);
						$user = ucwords($user);
						echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['authorizedby']) { echo " selected"; } echo ">".$user."</option>";
					}
				echo "</select></td>
		<td class='tdc'>"; if($rows['file1'] != '') echo "<a href='../uploads/".$rows['file1']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file2'] != '') echo "<a href='../uploads/".$rows['file2']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file3'] != '') echo "<a href='../uploads/".$rows['file3']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>
			<select name='uagent'>
				<option></option>";
				$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
				while($r = mysqli_fetch_array($q)) {
					$user = explode(".", $r['username']);
					$user = implode(" ", $user);
					$user = ucwords($user);
					echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['agent']) { echo " selected"; } echo ">".$user."</option>";
				}
			echo "</select></td>";
		echo "<td class='tdc'>".$rows['assigneddate']."</td>";
		echo "<td class='tdc'>".$rows['duedate']."</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>User Notes</td>
		<td class='tdh' colspan='3'>CDT Notes</td>
		<td class='tdh' colspan='3'>Final Notes</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2' valign='top'>" . $rows['user_notes'] . " <input type='hidden' name='uusernotes' value='".$rows['user_notes']."'/></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ucdtnotes'>".$rows['CDT_notes']."</textarea></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ufinalnotes'>" . $rows['final_notes'] . "</textarea></td>
	</tr>
	<tr style='background-color: #ccc;'>
		<td class='tdc' colspan='12' style='text-align: left;'>
			<input class='tdcbutton' type='submit' value='Update Ticket' style='float: right;'>
			</form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
            <a href='#show' id='show'>[Show]</a>
            <a href='#hide' id='hide'>/ [Hide]</a>
			<span id='cont' style='display: none;'>"; 
			$query2 = mysqli_query($conn, "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
			l.status AS status,	l.notes AS notes, t.duedate AS duedate, l.editdate AS editdate 
			FROM tickets_newtickets t, tickets_newticketlog l 
			WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'");
		echo "<table class='tab2' style='width:99%;'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $r2['datetaken'] . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $r2['duedate'] . "</td>
				<td class='tdc'>" . $r2['editdate'] . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>CDT Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			";  
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	";
}
mysqli_close($conn);
echo "</table>";
}

elseif(isset($_POST['sadsort'])) {
	$sagent = $_POST['sagent'];
	$sadsort = $_POST['sadsort'];
	$sstatus = $_POST['sstatus'];
	echo "<table class='tab'>";
	include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE agent = '".$sagent."' AND status = '".$sstatus."' ORDER BY submitteddate ".$sadsort."");
while($rows = mysqli_fetch_array($query)) {
	echo "<form action='suptickets.php' method='POST'>
	<tr>
		<td class='tdh'>ID</td>
		<td class='tdh'>Fullname</td>
		<td class='tdh'>E-Mail</td>
		<td class='tdh' colspan='2'>Course URL</td>
		<td class='tdh'>Class</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>Page</td>
	</tr>
	<tr>
			<td class='tdc'>" . $rows['id'] . "<input type='hidden' name='uid' value='".$rows['id']."'></td>
			<td class='tdc'>" . $rows['fullname'] . "<input type='hidden' name='ufullname' value='".$rows['fullname']."'></td>
			<td class='tdc'>" . $rows['email'] . "<input type='hidden' name='uemail' value='".$rows['email']."'></td>
			<td class='tdc' colspan='2'><input type='url' title='Please make sure you use https:// or http:// with your URL' name='ucourseurl' value='".$rows['courseurl']."'>
		<a href='".$rows['courseurl']."' target='_blank'>[URL]</a></td>
			<td class='tdc'>
				<select name='uclass'>";
					$q = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
					while($r = mysqli_fetch_array($q)) {
						echo "<option "; if ($r['classId'] == $rows['class']) { echo " selected"; } echo ">".$r['classId']."</option>";
					}
				echo "</select>
			</td>
			<td class='tdc'>".$rows['cname']."</td>
			<td class='tdc'>
				<select name='upage'>";
					echo "<option "; if ($rows['page'] == '') { echo " selected"; } echo "></option>";
					echo "<option "; if ($rows['page'] == 'Assessment') { echo " selected"; } echo ">Assessment</option>";
					echo "<option "; if ($rows['page'] == 'Assignment') { echo " selected"; } echo ">Assignment</option>";
					echo "<option title='Any page in course that is not an assignment, assessment, discussion or daily checkpoint' "; if ($rows['page'] == 'Content Page') { echo " selected"; } echo ">Content Page</option>";
					echo "<option "; if ($rows['page'] == 'Daily Checkpoint') { echo " selected"; } echo ">Daily Checkpoint</option>";
					echo "<option "; if ($rows['page'] == 'Discussion') { echo " selected"; } echo ">Discussion</option>";
				echo "</select>
			</td>
	</tr>
	<tr>
		<td class='tdh'>Program</td>
		<td class='tdh'>Week</td>
		<td class='tdh'>Issue</td>
		<td class='tdh' colspan='2'>Tier</td>
		<td class='tdh'>Priority</td>
		<td class='tdh'>Submitted Date</td>
		<td class='tdh'>Status</td>
	</tr>
	<tr>
		<td class='tdc'>
			<select name='uprogram'>
				<option></option>";
						$query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms") OR DIE("HAHA");

						while($row2 = mysqli_fetch_array($query2)) {
							echo "<option value='".$row2['program']."'"; if($rows['program'] == $row2['program']) {echo 'selected';} echo "> ".$row2['program']."</option>";
						}
				echo "
			</select>
		</td>
		<td class='tdc'>
			<select name='uweek' style='width:100%'>";
				echo "<option "; if ($rows['week'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['week'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['week'] == '3') { echo " selected"; } echo ">3</option>";
				echo "<option "; if ($rows['week'] == '4') { echo " selected"; } echo ">4</option>";
				echo "<option "; if ($rows['week'] == 'NA') { echo " selected"; } echo ">NA</option>";
			echo "</select>
		</td>
		</td>
		<td class='tdc'>
			<select name='uissue' style='width:100%'>";
				$q = mysqli_query($conn, "SELECT issue FROM tickets_issues ORDER BY issue ASC");
				while($r = mysqli_fetch_array($q)) {
					echo "<option "; if ($r['issue'] == $rows['issue']) { echo " selected"; } echo ">".$r['issue']."</option>";
				}
			echo "</select>
		</td>
		<td class='tdc'  colspan='2'>
			<select name='utier' style='width:100%'>";
				echo "<option "; if ($rows['tier'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['tier'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['tier'] == '3') { echo " selected"; } echo ">3</option>";
				
			echo "</select>
		</td>
		<td class='tdc'>
			<select name='upriority' style='width:100%'>";
				echo "<option value=''"; if ($rows['priority'] == '') { echo " selected"; } echo "></option>";
				echo "<option value='Low'"; if ($rows['priority'] == 'Low') { echo " selected"; } echo ">Low</option>";
				echo "<option value='Medium'"; if ($rows['priority'] == 'Medium') { echo " selected"; } echo ">Medium</option>";
				echo "<option value='High'"; if ($rows['priority'] == 'High') { echo " selected"; } echo ">High</option>";
			echo "</select>
		</td>
		<td class='tdc'>" . $rows['submitteddate'] . "<input type='hidden' name='usubmitteddate' value='".$rows['submitteddate']."'></td>
		<td class='tdc'>
			<select name='ustatus' style='width:100%'>
				<option value='Assigned' "; if($rows['status']=='Assigned') { echo " selected"; } echo ">Assigned</option>
				<option value='Completed' "; if($rows['status']=='Completed') { echo " selected"; } echo ">Completed</option>
				<option value='Pending' "; if($rows['status']=='Pending') { echo " selected"; } echo ">Pending</option>
				<option value='Rejected' "; if($rows['status']=='Rejected') { echo " selected"; } echo ">Rejected</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class='tdh'>Apply Change To</td>
		<td class='tdh'>Authorized by</td>
		<td class='tdh'>File 1</td>
		<td class='tdh'>File 2</td>
		<td class='tdh'>File 3</td>
		<td class='tdh'>Assigned To</td>
		<td class='tdh'>Assigned Date</td>
		<td class='tdh'>Due Date</td>
	</tr>
	<tr>
		<td class='tdc'><select name='uapplied' style='width: 100%;'>
			<option></option>
			<option"; if($rows['appliedto'] == 'Online') { echo " selected"; } echo ">Online</option>
			<option"; if($rows['appliedto'] == 'Blended') { echo " selected"; } echo ">Blended</option>
			<option"; if($rows['appliedto'] == 'Both') { echo " selected"; } echo ">Both</option>
			</select>
		</td>
		<td class='tdc'>
				<select name='uauth'>
					<option></option>";
					$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
					while($r = mysqli_fetch_array($q)) {
						$user = explode(".", $r['username']);
						$user = implode(" ", $user);
						$user = ucwords($user);
						echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['authorizedby']) { echo " selected"; } echo ">".$user."</option>";
					}
				echo "</select></td>
		<td class='tdc'>"; if($rows['file1'] != '') echo "<a href='../uploads/".$rows['file1']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file2'] != '') echo "<a href='../uploads/".$rows['file2']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file3'] != '') echo "<a href='../uploads/".$rows['file3']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>
			<select name='uagent'>
				<option></option>";
				$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
				while($r = mysqli_fetch_array($q)) {
					$user = explode(".", $r['username']);
					$user = implode(" ", $user);
					$user = ucwords($user);
					echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['agent']) { echo " selected"; } echo ">".$user."</option>";
				}
			echo "</select></td>";
		echo "<td class='tdc'>".$rows['assigneddate']."</td>";
		echo "<td class='tdc'>".$rows['duedate']."</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>User Notes</td>
		<td class='tdh' colspan='3'>CDT Notes</td>
		<td class='tdh' colspan='3'>Final Notes</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2' valign='top'>" . $rows['user_notes'] . " <input type='hidden' name='uusernotes' value='".$rows['user_notes']."'/></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ucdtnotes'>".$rows['CDT_notes']."</textarea></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ufinalnotes'>" . $rows['final_notes'] . "</textarea></td>
	</tr>
	<tr style='background-color: #ccc;'>
		<td class='tdc' colspan='12' style='text-align: left;'>
			<input class='tdcbutton' type='submit' value='Update Ticket' style='float: right;'></form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
            <a href='#show' id='show'>[Show]</a>
            <a href='#hide' id='hide'>/ [Hide]</a>
			<span id='cont' style='display: none;'>"; 
			$query2 = mysqli_query($conn, "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
			l.status AS status,	l.notes AS notes, t.duedate AS duedate, l.editdate AS editdate 
			FROM tickets_newtickets t, tickets_newticketlog l 
			WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'");
		echo "<table class='tab2' style='width:99%;'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $r2['datetaken'] . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $r2['duedate'] . "</td>
				<td class='tdc'>" . $r2['editdate'] . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>CDT Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			";  
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	";
}
mysqli_close($conn);
echo "</table>";
}	
elseif(isset($_POST['adsort'])) {
	$colsort = $_POST['colsort'];
	$adsort = $_POST['adsort'];
	echo "<table class='tab'>";
	include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets ORDER BY ".$colsort." ".$adsort."");
while($rows = mysqli_fetch_array($query)) {
	echo "<form action='suptickets.php' method='POST'>
	<tr>
		<td class='tdh'>ID</td>
		<td class='tdh'>Fullname</td>
		<td class='tdh'>E-Mail</td>
		<td class='tdh' colspan='2'>Course URL</td>
		<td class='tdh'>Class</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>Page</td>
	</tr>
	<tr>
			<td class='tdc'>" . $rows['id'] . "<input type='hidden' name='uid' value='".$rows['id']."'></td>
			<td class='tdc'>" . $rows['fullname'] . "<input type='hidden' name='ufullname' value='".$rows['fullname']."'></td>
			<td class='tdc'>" . $rows['email'] . "<input type='hidden' name='uemail' value='".$rows['email']."'></td>
			<td class='tdc' colspan='2'><input type='url' title='Please make sure you use https:// or http:// with your URL' name='ucourseurl' value='".$rows['courseurl']."'>
		<a href='".$rows['courseurl']."' target='_blank'>[URL]</a></td>
			<td class='tdc'>
				<select name='uclass'>";
					$q = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
					while($r = mysqli_fetch_array($q)) {
						echo "<option "; if ($r['classId'] == $rows['class']) { echo " selected"; } echo ">".$r['classId']."</option>";
					}
				echo "</select>
			</td>
			<td class='tdc'>".$rows['cname']."</td>
			<td class='tdc'>
				<select name='upage'>";
					echo "<option "; if ($rows['page'] == '') { echo " selected"; } echo "></option>";
					echo "<option "; if ($rows['page'] == 'Assessment') { echo " selected"; } echo ">Assessment</option>";
					echo "<option "; if ($rows['page'] == 'Assignment') { echo " selected"; } echo ">Assignment</option>";
					echo "<option title='Any page in course that is not an assignment, assessment, discussion or daily checkpoint' "; if ($rows['page'] == 'Content Page') { echo " selected"; } echo ">Content Page</option>";
					echo "<option "; if ($rows['page'] == 'Daily Checkpoint') { echo " selected"; } echo ">Daily Checkpoint</option>";
					echo "<option "; if ($rows['page'] == 'Discussion') { echo " selected"; } echo ">Discussion</option>";
				echo "</select>
			</td>
	</tr>
	<tr>
		<td class='tdh'>Program</td>
		<td class='tdh'>Week</td>
		<td class='tdh'>Issue</td>
		<td class='tdh' colspan='2'>Tier</td>
		<td class='tdh'>Priority</td>
		<td class='tdh'>Submitted Date</td>
		<td class='tdh'>Status</td>
	</tr>
	<tr>
		<td class='tdc'>
			<select name='uprogram'>
				<option></option>";
						$query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms") OR DIE("HAHA");

						while($row2 = mysqli_fetch_array($query2)) {
							echo "<option value='".$row2['program']."'"; if($rows['program'] == $row2['program']) {echo 'selected';} echo "> ".$row2['program']."</option>";
						}
				echo "
			</select>
		</td>
		<td class='tdc'>
			<select name='uweek' style='width:100%'>";
				echo "<option "; if ($rows['week'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['week'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['week'] == '3') { echo " selected"; } echo ">3</option>";
				echo "<option "; if ($rows['week'] == '4') { echo " selected"; } echo ">4</option>";
				echo "<option "; if ($rows['week'] == 'NA') { echo " selected"; } echo ">NA</option>";
			echo "</select>
		</td>
		</td>
		<td class='tdc'>
			<select name='uissue' style='width:100%'>";
				$q = mysqli_query($conn, "SELECT issue FROM tickets_issues ORDER BY issue ASC");
				while($r = mysqli_fetch_array($q)) {
					echo "<option "; if ($r['issue'] == $rows['issue']) { echo " selected"; } echo ">".$r['issue']."</option>";
				}
			echo "</select>
		</td>
		<td class='tdc'  colspan='2'>
			<select name='utier' style='width:100%'>";
				echo "<option "; if ($rows['tier'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['tier'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['tier'] == '3') { echo " selected"; } echo ">3</option>";
				
			echo "</select>
		</td>
		<td class='tdc'>
			<select name='upriority' style='width:100%'>";
				echo "<option value=''"; if ($rows['priority'] == '') { echo " selected"; } echo "></option>";
				echo "<option value='Low'"; if ($rows['priority'] == 'Low') { echo " selected"; } echo ">Low</option>";
				echo "<option value='Medium'"; if ($rows['priority'] == 'Medium') { echo " selected"; } echo ">Medium</option>";
				echo "<option value='High'"; if ($rows['priority'] == 'High') { echo " selected"; } echo ">High</option>";
			echo "</select>
		</td>
		<td class='tdc'>" . $rows['submitteddate'] . "<input type='hidden' name='usubmitteddate' value='".$rows['submitteddate']."'></td>
		<td class='tdc'>
			<select name='ustatus' style='width:100%'>
				<option value='Assigned' "; if($rows['status']=='Assigned') { echo " selected"; } echo ">Assigned</option>
				<option value='Completed' "; if($rows['status']=='Completed') { echo " selected"; } echo ">Completed</option>
				<option value='Pending' "; if($rows['status']=='Pending') { echo " selected"; } echo ">Pending</option>
				<option value='Rejected' "; if($rows['status']=='Rejected') { echo " selected"; } echo ">Rejected</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class='tdh'>Apply Change To</td>
		<td class='tdh'>Authorized by</td>
		<td class='tdh'>File 1</td>
		<td class='tdh'>File 2</td>
		<td class='tdh'>File 3</td>
		<td class='tdh'>Assigned To</td>
		<td class='tdh'>Assigned Date</td>
		<td class='tdh'>Due Date</td>
	</tr>
	<tr>
		<td class='tdc'><select name='uapplied' style='width: 100%;'>
			<option></option>
			<option"; if($rows['appliedto'] == 'Online') { echo " selected"; } echo ">Online</option>
			<option"; if($rows['appliedto'] == 'Blended') { echo " selected"; } echo ">Blended</option>
			<option"; if($rows['appliedto'] == 'Both') { echo " selected"; } echo ">Both</option>
			</select>
		</td>
		<td class='tdc'>
				<select name='uauth'>
					<option></option>";
					$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
					while($r = mysqli_fetch_array($q)) {
						$user = explode(".", $r['username']);
						$user = implode(" ", $user);
						$user = ucwords($user);
						echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['authorizedby']) { echo " selected"; } echo ">".$user."</option>";
					}
				echo "</select></td>
		<td class='tdc'>"; if($rows['file1'] != '') echo "<a href='../uploads/".$rows['file1']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file2'] != '') echo "<a href='../uploads/".$rows['file2']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file3'] != '') echo "<a href='../uploads/".$rows['file3']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>
			<select name='uagent'>
				<option></option>";
				$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
				while($r = mysqli_fetch_array($q)) {
					$user = explode(".", $r['username']);
					$user = implode(" ", $user);
					$user = ucwords($user);
					echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['agent']) { echo " selected"; } echo ">".$user."</option>";
				}
			echo "</select></td>";
		echo "<td class='tdc'>".$rows['assigneddate']."</td>";
		echo "<td class='tdc'>".$rows['duedate']."</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>User Notes</td>
		<td class='tdh' colspan='3'>CDT Notes</td>
		<td class='tdh' colspan='3'>Final Notes</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2' valign='top'>" . $rows['user_notes'] . " <input type='hidden' name='uusernotes' value='".$rows['user_notes']."'/></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ucdtnotes'>".$rows['CDT_notes']."</textarea></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ufinalnotes'>" . $rows['final_notes'] . "</textarea></td>
	</tr>
	<tr style='background-color: #ccc;'>
		<td class='tdc' colspan='12' style='text-align: left;'>
			<input class='tdcbutton' type='submit' value='Update Ticket' style='float: right;'></form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
            <a href='#show' id='show'>[Show]</a>
            <a href='#hide' id='hide'>/ [Hide]</a>
			<span id='cont' style='display: none;'>"; 
			$query2 = mysqli_query($conn, "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
			l.status AS status,	l.notes AS notes, t.duedate AS duedate, l.editdate AS editdate 
			FROM tickets_newtickets t, tickets_newticketlog l 
			WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'");
		echo "<table class='tab2' style='width:99%;'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $r2['datetaken'] . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $r2['duedate'] . "</td>
				<td class='tdc'>" . $r2['editdate'] . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>CDT Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			";  
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	";
}
mysqli_close($conn);
echo "</table>";
}
else {
echo "<table class='tab' border:none;>";
include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE submitteddate >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY submitteddate ASC");
while($rows = mysqli_fetch_array($query)) {
	echo "<form action='suptickets.php' method='POST'>
	<tr>
		<td class='tdh'>ID</td>
		<td class='tdh'>Fullname</td>
		<td class='tdh'>E-Mail</td>
		<td class='tdh' colspan='2'>Course URL</td>
		<td class='tdh'>Class</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>Page</td>
	</tr>
	<tr>
			<td class='tdc'>" . $rows['id'] . "<input type='hidden' name='uid' value='".$rows['id']."'></td>
			<td class='tdc'>" . $rows['fullname'] . "<input type='hidden' name='ufullname' value='".$rows['fullname']."'></td>
			<td class='tdc'>" . $rows['email'] . "<input type='hidden' name='uemail' value='".$rows['email']."'></td>
			<td class='tdc' colspan='2'><input type='url' title='Please make sure you use https:// or http:// with your URL' name='ucourseurl' value='".$rows['courseurl']."'>
		<a href='".$rows['courseurl']."' target='_blank'>[URL]</a></td>
			<td class='tdc'>
				<select name='uclass'>";
					$q = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
					while($r = mysqli_fetch_array($q)) {
						echo "<option "; if ($r['classId'] == $rows['class']) { echo " selected"; } echo ">".$r['classId']."</option>";
					}
				echo "</select>
			</td>
			<td class='tdc'>".$rows['cname']."</td>
			<td class='tdc'>
				<select name='upage'>";
					echo "<option "; if ($rows['page'] == '') { echo " selected"; } echo "></option>";
					echo "<option "; if ($rows['page'] == 'Assessment') { echo " selected"; } echo ">Assessment</option>";
					echo "<option "; if ($rows['page'] == 'Assignment') { echo " selected"; } echo ">Assignment</option>";
					echo "<option title='Any page in course that is not an assignment, assessment, discussion or daily checkpoint' "; if ($rows['page'] == 'Content Page') { echo " selected"; } echo ">Content Page</option>";
					echo "<option "; if ($rows['page'] == 'Daily Checkpoint') { echo " selected"; } echo ">Daily Checkpoint</option>";
					echo "<option "; if ($rows['page'] == 'Discussion') { echo " selected"; } echo ">Discussion</option>";
				echo "</select>
			</td>
	</tr>
	<tr>
		<td class='tdh'>Program</td>
		<td class='tdh'>Week</td>
		<td class='tdh'>Issue</td>
		<td class='tdh' colspan='2'>Tier</td>
		<td class='tdh'>Priority</td>
		<td class='tdh'>Submitted Date</td>
		<td class='tdh'>Status</td>
	</tr>
	<tr>
		<td class='tdc'>
			<select name='uprogram'>
				<option></option>";
						$query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms") OR DIE("HAHA");

						while($row2 = mysqli_fetch_array($query2)) {
							echo "<option value='".$row2['program']."'"; if($rows['program'] == $row2['program']) {echo 'selected';} echo "> ".$row2['program']."</option>";
						}
				echo "
			</select>
		</td>
		<td class='tdc'>
			<select name='uweek' style='width:100%'>";
				echo "<option "; if ($rows['week'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['week'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['week'] == '3') { echo " selected"; } echo ">3</option>";
				echo "<option "; if ($rows['week'] == '4') { echo " selected"; } echo ">4</option>";
				echo "<option "; if ($rows['week'] == 'NA') { echo " selected"; } echo ">NA</option>";
			echo "</select>
		</td>
		</td>
		<td class='tdc'>
			<select name='uissue' style='width:100%'>";
				$q = mysqli_query($conn, "SELECT issue FROM tickets_issues ORDER BY issue ASC");
				while($r = mysqli_fetch_array($q)) {
					echo "<option "; if ($r['issue'] == $rows['issue']) { echo " selected"; } echo ">".$r['issue']."</option>";
				}
			echo "</select>
		</td>
		<td class='tdc'  colspan='2'>
			<select name='utier' style='width:100%'>";
				echo "<option "; if ($rows['tier'] == '1') { echo " selected"; } echo ">1</option>";
				echo "<option "; if ($rows['tier'] == '2') { echo " selected"; } echo ">2</option>";
				echo "<option "; if ($rows['tier'] == '3') { echo " selected"; } echo ">3</option>";
				
			echo "</select>
		</td>
		<td class='tdc'>
			<select name='upriority' style='width:100%'>";
				echo "<option value=''"; if ($rows['priority'] == '') { echo " selected"; } echo "></option>";
				echo "<option value='Low'"; if ($rows['priority'] == 'Low') { echo " selected"; } echo ">Low</option>";
				echo "<option value='Medium'"; if ($rows['priority'] == 'Medium') { echo " selected"; } echo ">Medium</option>";
				echo "<option value='High'"; if ($rows['priority'] == 'High') { echo " selected"; } echo ">High</option>";
			echo "</select>
		</td>
		<td class='tdc'>" . $rows['submitteddate'] . "<input type='hidden' name='usubmitteddate' value='".$rows['submitteddate']."'></td>
		<td class='tdc'>
			<select name='ustatus' style='width:100%'>
				<option value='Assigned' "; if($rows['status']=='Assigned') { echo " selected"; } echo ">Assigned</option>
				<option value='Completed' "; if($rows['status']=='Completed') { echo " selected"; } echo ">Completed</option>
				<option value='Pending' "; if($rows['status']=='Pending') { echo " selected"; } echo ">Pending</option>
				<option value='Rejected' "; if($rows['status']=='Rejected') { echo " selected"; } echo ">Rejected</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class='tdh'>Apply Change To</td>
		<td class='tdh'>Authorized by</td>
		<td class='tdh'>File 1</td>
		<td class='tdh'>File 2</td>
		<td class='tdh'>File 3</td>
		<td class='tdh'>Assigned To</td>
		<td class='tdh'>Assigned Date</td>
		<td class='tdh'>Due Date</td>
	</tr>
	<tr>
		<td class='tdc'><select name='uapplied' style='width: 100%;'>
			<option></option>
			<option"; if($rows['appliedto'] == 'Online') { echo " selected"; } echo ">Online</option>
			<option"; if($rows['appliedto'] == 'Blended') { echo " selected"; } echo ">Blended</option>
			<option"; if($rows['appliedto'] == 'Both') { echo " selected"; } echo ">Both</option>
			</select>
		</td>
		<td class='tdc'>
				<select name='uauth'>
					<option></option>";
					$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
					while($r = mysqli_fetch_array($q)) {
						$user = explode(".", $r['username']);
						$user = implode(" ", $user);
						$user = ucwords($user);
						echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['authorizedby']) { echo " selected"; } echo ">".$user."</option>";
					}
				echo "</select></td>
		<td class='tdc'>"; if($rows['file1'] != '') echo "<a href='../uploads/".$rows['file1']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file2'] != '') echo "<a href='../uploads/".$rows['file2']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file3'] != '') echo "<a href='../uploads/".$rows['file3']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>
			<select name='uagent'>
				<option></option>";
				$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
				while($r = mysqli_fetch_array($q)) {
					$user = explode(".", $r['username']);
					$user = implode(" ", $user);
					$user = ucwords($user);
					echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['agent']) { echo " selected"; } echo ">".$user."</option>";
				}
			echo "</select></td>";
		echo "<td class='tdc'>".$rows['assigneddate']."</td>";
		echo "<td class='tdc'>".$rows['duedate']."</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>User Notes</td>
		<td class='tdh' colspan='3'>CDT Notes</td>
		<td class='tdh' colspan='3'>Final Notes</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2' valign='top'>" . $rows['user_notes'] . " <input type='hidden' name='uusernotes' value='".$rows['user_notes']."'/></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ucdtnotes'>".$rows['CDT_notes']."</textarea></td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ufinalnotes'>" . $rows['final_notes'] . "</textarea></td>
	</tr>
	<tr style='background-color: #ccc;'>
		<td class='tdc' colspan='12' style='text-align: left;'>
			<input class='tdcbutton' type='submit' value='Update Ticket' style='float: right;'></form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
            <a href='#show' id='show'>[Show]</a>
            <a href='#hide' id='hide'>/ [Hide]</a>
			<span id='cont' style='display: none;'>"; 
			$query2 = mysqli_query($conn, "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
			l.status AS status,	l.notes AS notes, t.duedate AS duedate, l.editdate AS editdate 
			FROM tickets_newtickets t, tickets_newticketlog l 
			WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'");
		echo "<table class='tab2' style='width:99%;'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $r2['datetaken'] . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $r2['duedate'] . "</td>
				<td class='tdc'>" . $r2['editdate'] . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>CDT Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			";  
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	";
}
mysqli_close($conn);
echo "</table>";
}
?>


</div>
</content>

</body>
</html>