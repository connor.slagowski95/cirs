<?php 
session_start();
include("../includes/security.inc");
security("max");
ini_set("SMTP", "10.50.2.200");
?>
<!DOCTYPE html>
<html lang="en">


<head>
<link rel="stylesheet" href="../includes/styles.css">
</head>

<?php
if(isset($_POST['pw'])) {
	$un = strtolower($_POST['adminfname']) . '.' . strtolower($_POST['adminlname']);
	$pw = $_POST['pw'];
	$email = strtolower($_POST['email']);

	if($_POST['clearance'] == 'basic') {
		$basic = 'x';
		$edit = '';
		$supervisor = '';
		$max = '';
		$ADC = '';
	}
	if($_POST['clearance'] == 'edit') {
		$basic = 'x';
		$edit = 'x';
		$supervisor = '';
		$max = '';
		$ADC = 'x';
	}
	if($_POST['clearance'] == 'supervisor') {
		$basic = 'x';
		$edit = 'x';
		$supervisor = 'x';
		$max = '';
		$ADC = '';
	}
	if($_POST['clearance'] == 'ADC') {
		$basic = 'x';
		$edit = '';
		$supervisor = '';
		$max = '';
		$ADC = 'x';
	}
	if($_POST['clearance'] == 'max') {
		$basic = 'x';
		$edit = 'x';
		$supervisor = 'x';
		$max = 'x';
		$ADC = 'x';
	}
			
	
	include("../../live_connect/connect.inc");
	$query = mysqli_query($conn, "INSERT INTO tickets_admin(username, password, email, basic, edit, max, supervisor, ADC) VALUES('".$un."', '".$pw."', '".$email."', 
	'".$basic."', '".$edit."', '".$max."', '".$supervisor."', '".$ADC."')");
	mysqli_close($conn);

	$to = $_POST['email'];
	$subject = "CDT Ticket System Credentials";
	$txt = "Hi, " . $_POST['adminfname'] . "
	We have just added you to the CDT Ticket system. 
	The credentials to sign in are:\n
	Username: " . $un . "
	Password: " . $pw . "
	Direct link: https://online-shc.com/arc/admin/cdttickets";
	$headers = "From: david.cowesrt@independence.edu" . "\r\n";

	mail($to,$subject,$txt,$headers);

}

if(isset($_POST['epassword'])) {
	
	if($_POST['eusername'] != '' && $_POST['epassword'] != '')
	{
		$eusername = strtolower($_POST['eusername']);
		$eoldusername = $_POST['eoldusername'];
		$epassword = $_POST['epassword'];
		$eemail = strtolower($_POST['eemail']);
		
		if($_POST['eclearance'] == 'basic') {
			$basic = 'x';
			$edit = '';
			$supervisor = '';
			$max = '';
			$ADC = '';
		}
		if($_POST['eclearance'] == 'edit') {
			$basic = 'x';
			$edit = 'x';
			$supervisor = '';
			$max = '';
			$ADC = 'x';
		}
		if($_POST['eclearance'] == 'supervisor') {
			$basic = 'x';
			$edit = 'x';
			$supervisor = 'x';
			$max = '';
			$ADC = '';
		}
		if($_POST['eclearance'] == 'ADC') {
			$basic = 'x';
			$edit = '';
			$supervisor = '';
			$max = '';
			$ADC = 'x';
		}
		if($_POST['eclearance'] == 'max') {
			$basic = 'x';
			$edit = 'x';
			$supervisor = 'x';
			$max = 'x';
			$ADC = 'x';
		}
		
		$upassword = password_hash($epassword, PASSWORD_BCRYPT);
		
		include("../../live_connect/connect.inc");
		$query = mysqli_query($conn, "UPDATE tickets_admin SET username = '".$eusername."', password = '".$upassword."', 
		email = '".$eemail."', basic = '".$basic."', edit = '".$edit."', max = '".$max."', 
		supervisor = '".$supervisor."', ADC = '".$ADC."'
		WHERE username = '".$eoldusername."'");
		mysqli_close($conn);
		
		
		if($_POST['eoldemail'] != $_POST['eemail'] || $_POST['eoldusername'] != $_POST['eusername'] ||
		$_POST['eoldpassword'] != $_POST['epassword']) {
			$to = $_POST['eemail'];
			$subject = "CDT Ticket System Credentials";
			$txt = "Hi, " . $_POST['eusername'] . "
			We have just updated your details in the CDT Ticket system. 
			The credentials to sign in are:\n
			Username: " . $eusername . "
			Password: " . $epassword . "
			Direct link: https://online-shc.com/arc/admin/cdttickets";
			$headers = "From: david.cowesrt@independence.edu" . "\r\n";

			mail($to,$subject,$txt,$headers);
		}
	}
}

if(isset($_POST['deleteusername'])) {
	if($_POST['deleteusername'] != '') {
		include("../../live_connect/connect.inc");
		$deleteusername = $_POST['deleteusername'];
		mysqli_query($conn, "DELETE FROM tickets_admin WHERE username = '".$deleteusername."'");
		mysqli_close($conn);
	}
}
?>
<body id='main_page'>
        <nav class='menu'>
            <?php include_once('../includes/adminmenu.php') ?>
        </nav>

        <content id='content'>
		<div class='headingArea'>Admin Edit</div>
		<div class='page'>
<form action="adminedit.php" method="POST">
<table>
	<tr><td class='tdh'>Forename:</td><td><input type="text" name="adminfname" required></td></tr>
	<tr><td class='tdh'>Surname:</td><td><input type="text" name="adminlname" required></td></tr>
	<tr><td class='tdh'>Password:</td><td><input type="password" name="pw" required></td></tr>
	<tr><td class='tdh'>E-Mail:</td><td><input type="text" name="email" required></td></tr>
	<tr><td colspan='2'>Choose a level of clearance below:</td></tr>
	<tr><td class='tdh'><label for="basic">Basic</label></td><td><input type="radio" name="clearance" id="basic" value='basic' required></td></tr>
	<tr><td class='tdh'><label for="ADC">ADC</label></td><td><input type="radio" name="clearance" id="ADC"  value='ADC' required></td></tr>
	<tr><td class='tdh'><label for="editable">Editable</label></td><td><input type="radio" name="clearance" id="editable"  value='edit' required></td></tr>
	<tr><td class='tdh'><label for="max">Maximum</label></td><td><input type="radio" name="clearance" id='max' value='max' required></td></tr>
	
	<tr><td class='tdb' colspan="2"><input type="submit" value="Add Admin"></td></tr>
</table>
</form>
</div>

<div class="headingArea">
Admin List
</div>
<div class='page'>
<?php
include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_admin ORDER BY username ASC");
while($rows = mysqli_fetch_array($query)) {
	echo "<div class='adminedit' style = 'float:left; padding-right: 15px; padding-bottom:15px;'>
	<form action='adminedit.php' method='POST'>
	<table style='border-collapse: collapse;'>
		<tr><td class='tdh'>Username:</td><td><input type='text' name='eusername' value='" . $rows['username'] . "'>
			<input type='hidden' name='eoldusername' value='".$rows['username']."'></td></tr>
		<tr><td class='tdh'>Password:</td><td><input type='password' name='epassword' value='" . $rows['password'] . "'>
			<input type='hidden' name='eoldpassword' value='".$rows['password']."'></td></tr>
		<tr><td class='tdh'>E-Mail:</td><td><input type='text' name='eemail' value='" . $rows['email'] . "'>
			<input type='hidden' name='eoldemail' value='".$rows['email']."'></td></tr>

			<tr><td class='tdh'><label for='basic'>Basic</label></td><td><input type='radio' name='eclearance' id='basic' value='basic'
				"; if($rows['basic'] == 'x' && $rows['ADC'] == '' && $rows['edit'] == '' && $rows['max'] == '') {echo 'checked';} echo " required></td></tr>
			<tr><td class='tdh'><label for='ADC'>ADC</label></td><td><input type='radio' name='eclearance' id='ADC' value='ADC'
				"; if($rows['basic'] == 'x' && $rows['ADC'] == 'x' && $rows['edit'] == '' && $rows['max'] == '') {echo 'checked';} echo " required></td></tr>
			<tr><td class='tdh'><label for='editable'>Editable</label></td><td><input type='radio' name='eclearance' id='editable' value='edit'
				"; if($rows['basic'] == 'x' && $rows['ADC'] == 'x' && $rows['edit'] == 'x' && $rows['max'] == '') {echo 'checked';} echo " required></td></tr>
			<tr><td class='tdh'><label for='max'>Maximum</label></td><td><input type='radio' name='eclearance' id='max' value='max' 
				"; if($rows['basic'] == 'x' && $rows['ADC'] == 'x' && $rows['edit'] == 'x' && $rows['max'] == 'x') {echo 'checked';} echo " required></td></tr>

	<tr><td style='height: 15px;' colspan='2'></td></tr>
	<tr><td class='tdb' colspan='2'><input style='float: right;' type='submit' value='Update'></form>
	<form action='adminedit.php' method='POST'>
	<input type='hidden' name='deleteusername' value='".$rows['username']."'>
	<input style='float: left;' type='submit' value='Delete'>
	</form></td></tr>
</table></div>";
}
mysqli_close($conn);
?>

</div>
</content>

</body>
</html>