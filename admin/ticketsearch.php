<?php
session_start([
    'cookie_lifetime' => 86400,
]); 
include("../includes/security.inc");
security("basic");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page title</title>
    <link rel="stylesheet" href="../includes/styles.css">

    <style>
        table {
            border: 1px solid lightgrey;
        }
        .tdh {
            text-align:left;
            color: #FFFFFF;
            background-color: #013245;
            border: none;
            padding-left: 5px;
        }
        .tdc {
            border: solid lightgray;
            border-width: 1px 1px 0px 0px;
            padding: 5px 5px;
        }
        .dheader {
            display: none;

        }
        .details {
            display: none;
        }


        /*search styles*/
        #form {
            grid-row: 2/3;
            grid-column: 1/-1;
        }
        #container {
            display: grid;
            grid-column: 1/-1;
            grid-row: 3/4;
            grid-template-columns: repeat(1, 1fr);
            grid-template-rows: 20px 1fr 30px;
        }
        #message {
            grid-row: 1/2;
        }
        #search_btns {
            grid-row: 2/3;
            grid-column: 1/-1;
        }
        .reports {
            grid-row: 3/4;
            grid-column: 1/-1;
        }
        .quick_search {
            float: left;
            width: 100px;
            height: 100px;
            border-radius: 15px;
            padding: 5px;
            margin: 5px;
        }
        .count {
            text-align: right;
            margin: 0px;
            font-size: 40pt;
            font-weight: bold;
        }
        .title {
            margin: 0px;
        }
        
    </style>
    <?php
    //pagination
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        }
        else {
            $pageno = 1;
        }

        $no_of_records_per_page = 40;
        $offset = ($pageno-1) * $no_of_records_per_page; 
        include("../../live_connect/connect.inc");
        $message = "";
    
    //search queries
    if(isset($_GET['subtest']) && $_GET['subtest'] != '' && !isset($_GET['duedate'])) {
        
        foreach(array_slice($_GET, 1) as $key=>$val) {
            if($val != "" && strlen($val) > 0) {
                $ssdate = $_GET['submittedsdate'];
                $sedate = $_GET['submittededate'];
                $csdate = $_GET['completedsdate'];
                $cedate = $_GET['completededate'];

                if ($key == 'submittedsdate') {
                    continue;
                }
                if ($key == 'completedsdate') {
                    continue;
                }
                if ($key == 'duedate') {
                    continue;
                }

                if($val == $sedate) {
                    $q .= " submitteddate BETWEEN '".$ssdate."' AND '".$sedate."' AND ";
                }
                elseif($val == $cedate) {
                    $q .= " completeddate BETWEEN '".$csdate."' AND '".$cedate."' AND ";
                }
                elseif($val == $_GET['id']) {
                    $q .= " id = '".$_GET['id']."' AND ";
                }
                else {
                    $q .= $key . " LIKE '%".$val."%' AND ";
                }
            }
        }
        if(isset($_GET['pageno'])) {
            $q = substr($q, 0, strlen($q) - 27);
        }
        else {
            $q = substr($q, 0, strlen($q) - 5);
        }
            $q = htmlspecialchars($q);

        if($q != '') {
            $sqlquerycount = "SELECT COUNT(*) FROM tickets_newtickets WHERE $q ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";
        }
        else {
            $sqlquerycount="SELECT COUNT(*) FROM tickets_newtickets ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";
        }
        $result = mysqli_query($conn, $sqlquerycount) OR DIE("HAHA");
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows/$no_of_records_per_page);

        if($q != '') {
            $sql="SELECT * FROM tickets_newtickets WHERE $q ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";
            $message = 'You may have to click the restart button mupltiple times to reset the search parameters';
        }
        else {
            $sql="SELECT * FROM tickets_newtickets ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";
            $message = 'There must be at least one search parameter chosen before you can search';

        }
    }
    elseif(isset($_GET['subtest']) && isset($_GET['duedate'])) {
        $sqlquerycount = "SELECT COUNT(*) FROM tickets_newtickets WHERE duedate < CURDATE() AND duedate != '0000-00-00' ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";
        $result = mysqli_query($conn, $sqlquerycount) OR DIE("HAHA");
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows/$no_of_records_per_page);

        $sql="SELECT * FROM tickets_newtickets WHERE duedate < CURDATE() AND duedate != '0000-00-00' ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";

        $message = 'You may have to click the restart button mupltiple times to reset the search parameters';
    }
    elseif(isset($_GET['restart'])) {
        $total_pages_sql = "SELECT COUNT(*) FROM tickets_newtickets";
        $result = mysqli_query($conn,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);

        $sql = "SELECT * FROM tickets_newtickets ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";
    }
    else {
        $total_pages_sql = "SELECT COUNT(*) FROM tickets_newtickets ORDER BY id ASC";
        $result = mysqli_query($conn,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);

        $sql = "SELECT * FROM tickets_newtickets ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";

        $message = 'You may have to click the restart button mupltiple times to reset the search parameters';
    }
?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            $(document).ready(function (){
                $("#restart").hover(function (){
                    $(this).css("background-color", "black");
                    $(this).css("color", "white");
                    }, function (){
                    $(this).css("background-color", "white");
                    $(this).css("color", "black");
                });
                
                $("#pend").hover(function (){
                    $(this).css("background-color", "yellow");
                    }, function (){
                    $(this).css("background-color", "#dbcf1f");
                });
                $("#pendweek").hover(function (){
                    $(this).css("background-color", "yellow");
                    }, function (){
                    $(this).css("background-color", "#dbcf1f");
                });
               

                $("#assn").hover(function (){
                    $(this).css("background-color", "lightgreen");
                    }, function (){
                    $(this).css("background-color", "green");
                });
                $("#assnweek").hover(function (){
                    $(this).css("background-color", "lightgreen");
                    }, function (){
                    $(this).css("background-color", "green");
                });

                $("#duedate").hover(function (){
                    $(this).css("background-color", "#FFCC00");
                    }, function (){
                    $(this).css("background-color", "orange");
                });

                $("#compweek").hover(function (){
                    $(this).css("background-color", "#FF5D59");
                    }, function (){
                    $(this).css("background-color", "red");
                });
                $("#compmonth").hover(function (){
                    $(this).css("background-color", "#FF5D59");
                    }, function (){
                    $(this).css("background-color", "red");
                });

                $(".expand_btn~label").hover(function (){
                    $(this).css("background-color", "lightgrey");
                    }, function (){
                    $(this).css("background-color", "grey");
                });

                <?php
                
                $q = mysqli_query($conn, $sql); 
                while($r = mysqli_fetch_array($q)) {
                ?>
                $("#expand<?php echo $r['id'] ?>").click(function () {
                    if($(this).prop("checked") == false) {
                        $("#detail<?php echo $r['id'] ?>").css("display", "none");
                        $("#dheader<?php echo $r['id'] ?>").css("display", "none");
                        $("#dtheader<?php echo $r['id'] ?>").css("display", "none");
                        $("#dt<?php echo $r['id'] ?>").css("display", "none");
                        $("#adinfoheader<?php echo $r['id'] ?>").css("display", "none");
                        $("#adinfo<?php echo $r['id'] ?>").css("display", "none");
                    }
                    else if($(this).prop("checked") == true) {
                        $("#detail<?php echo $r['id'] ?>").css("display", "table-row");
                        $("#dheader<?php echo $r['id'] ?>").css("display", "table-row");
                        $("#dtheader<?php echo $r['id'] ?>").css("display", "table-row");
                        $("#dt<?php echo $r['id'] ?>").css("display", "table-row");
                        $("#adinfoheader<?php echo $r['id'] ?>").css("display", "table-row");
                        $("#adinfo<?php echo $r['id'] ?>").css("display", "table-row");
                    }
                });
                <?php
                }
                ?>

                $("#ccode").change(function() {
                    var classtype = $("#ccode option:selected").val();
                        if(classtype == 'cname') {
                            $("#class").attr('name', 'cname');
                        }
                        else if(classtype == 'class') {
                            $("#class").attr('name', 'class');
                        }
                    $.ajax({
                        type:"POST",
                        url:"class_dropdown.php",
                        data: { ccode: classtype }
                    }).done(function(data) {
                        $("#class").html(data);
                    });

                    $.ajax({
                        type:"POST",
                        url:"class_title.php",
                        data: { ccode: classtype }
                    }).done(function(data) {
                        $("#title").html(data);
                    });

                    if(classtype == "class") {
                        $("#class").css("width", "100%");
                    }  
                    if(classtype == "cname") {
                        $("#authorizedby").css("width", "100%");
                    }
                });
                $("#pend").click(function () {
                    $("#pendform").submit();
                });
                $("#pendweek").click(function () {
                    $("#pendweekform").submit();
                });
                $("#assn").click(function () {
                    $("#assnform").submit();
                });
                $("#assnweek").click(function () {
                    $("#assnweekform").submit();
                });
                $("#duedate").click(function () {
                    $("#duedateform").submit();
                });
                $("#compweek").click(function() {
                    $("#compweekform").submit();
                });
                $("#compmonth").click(function() {
                    $("#compmonthform").submit();
                });
                $("#restart").click(function() {
                    $("#resetform").submit();

                    $.ajax("clearSession.php");
                });

        <?php
            include("../../live_connect/connect.inc");
            $q = mysqli_query($conn, $sql); 
            while($r = mysqli_fetch_array($q)) {
        ?>
            document.GETElementById("expand<?php echo $r['id'] ?>").onclick = () => sendGETMessage();
        <?php
            }
        ?>
                
            });
        </script>

</head>
<?php
        if(isset($_GET['subtest'])) {
           foreach($_GET as $name => $vals) {
                $params = $name. "=" . $vals; 
                $parms = $parms . "&" . $params;
            }
            $parms = $parms . "&";
            $parms = substr($parms, 1);
        }
            
        ?>
<body id='main_page'>
    <nav class='menu'>
        <?php include_once('../includes/adminmenu.php') ;
        
        
        ?>
    </nav>
    <content class='content' id='content'>
    
        <div class='headingArea'>Ticket Search</div>

        <div class='page'>
        <div id='form'>
                <form action='ticketsearch.php' method='GET' >
                <input type='hidden' name='subtest' value='subtest'/>
                    <table style='border: none;'>
                        <tr>
                            <td>Ticket ID:</td>
                            <td>Class Type:</td>
                            <td id='title'>
                            <?php
                                if(isset($_GET['class'])) {
                                    echo 'Class Code: ';
                                }
                                elseif(isset($_GET['cname'])) {
                                    echo 'Class Name: ';
                                }
                                else {
                                    echo 'Class Name or Code: ';
                                }
                            ?>
                            </td>
                            <td>Program:</td>
                            <td>Page:</td>
                            <td>Week:</td>
                            <td>Issue:</td>
                            <td>Tier:</td>
                            
                        </tr>
                        <tr>
                            <td>
                                <input type='text' name='id'
                                    <?php if(isset($_GET['id'])) {
                                            echo " value = '".$_GET['id']."'";
                                    } ?>
                                />
                            </td>
                            <td>
                                <select name="" id="ccode" style='width:100%'>
                                    <option value="select"></option>
                                    <option value="cname">Class Name</option>
                                    <option value="class">Class Code</option>
                                </select>
                            </td>
                            <td>
                            <span id='classType' style='width: 100%;'>
                                <?php
                                    if(isset($_GET['class'])) {
                                        include("../../live_connect/connect.inc");
                                        $q = mysqli_query($conn, "SELECT DISTINCT class FROM tickets_newtickets ORDER BY class ASC");
                                        echo '
                                                <select name="" id="class" style="width:100%;">
                                                    <option></option>';
                                                while($r = mysqli_fetch_array($q)) {
                                                    echo '<option value="'.$r['class'].'"'; if($_GET['class'] == $r['class']) { echo "selected"; } echo'> ' . 
                                                    $r['class'] . '</option>';
                                                }
                                                echo '</select>
                                            
                                        ';
                                    }
                                    elseif(isset($_GET['cname'])) {
                                        include("../../live_connect/connect.inc");
                                        $q = mysqli_query($conn, "SELECT DISTINCT cname FROM tickets_newtickets ORDER BY cname ASC");
                                        echo '
                                                <select name="" id="cname" style="width:100%;">
                                                    <option></option>';
                                                while($r = mysqli_fetch_array($q)) {
                                                    echo '<option value="'.$r['cname'].'"'; if($_GET['cname'] == $r['cname']) { echo "selected"; } echo'> ' . $r['cname'] . '</option>';
                                                }
                                                echo '</select>
                                        ';
                                    }
                                    else {
                                        echo '
                                                <select name="" id="class" style="width:100%;">
                                                    <option value="">Select a Class Type</option>
                                                </select>
                                        ';
                                    }
                                ?>
                                </span>
                            </td>
                            <td>
                                <select name="program" id="">
                                    <option value=""></option>
                                    <?php
                                        include("../../live_connect/connect.inc");
                                        $query = mysqli_query($conn, "SELECT * FROM SchoolPrograms");
                                        while($rows = mysqli_fetch_array($query)) {
                                            echo "<option value='".$rows['program']."'";
                                                if(isset($_GET['program']) && $_GET['program'] == $rows['program']) { echo "selected"; } echo "
                                            >".$rows['program']."</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <select name="page" style='width: 100%;'>
                                    <option value=""></option>
                                    <?php
                                        include("../../live_connect/connect.inc");
                                        $query = mysqli_query($conn, "SELECT DISTINCT page FROM tickets_newtickets ORDER BY page ASC");
                                        while($rows = mysqli_fetch_array($query)) {
                                            if($rows['page'] != "" && $rows['page'] != ' ') {
                                                echo "<option value='".$rows['page']."' ";
                                                    if(isset($_GET['page']) && $_GET['page'] == $rows['page']) { echo "selected"; } echo "
                                                >";
                                                    echo $rows['page'];
                                                echo "</option>";
                                            }
                                        }
                                    ?>
                                </select>
                                <td>
                                <select name="week" style="width: 100%;">
                                    <option value=""></option>
                                    <?php
                                        include("../../live_connect/connect.inc");
                                        $query = mysqli_query($conn, "SELECT DISTINCT week FROM tickets_newtickets ORDER BY week ASC");
                                        while($rows = mysqli_fetch_array($query)) {
                                            echo "<option value='".$rows['week']."'";
                                            if(isset($_GET['week']) && $_GET['week'] == $rows['week']) { echo "selected"; } echo "
                                            >".$rows['week']."</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <select name="issue">
                                    <option value=""></option>
                                    <?php
                                        include("../../live_connect/connect.inc");
                                        $query = mysqli_query($conn, "SELECT DISTINCT issue FROM tickets_newtickets ORDER BY issue ASC");
                                        while($rows = mysqli_fetch_array($query)) {
                                            echo "<option value='".$rows['issue']."'";
                                            if(isset($_GET['issue']) && $_GET['issue'] == $rows['issue']) { echo "selected"; } echo "
                                            >".$rows['issue']."</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <select name="tier">
                                    <option value=""></option>
                                    <?php
                                        include("../../live_connect/connect.inc");
                                        $query = mysqli_query($conn, "SELECT DISTINCT tier FROM tickets_newtickets ORDER BY tier ASC");
                                        while($rows = mysqli_fetch_array($query)) {
                                            echo "<option value='".$rows['tier']."'";
                                            if(isset($_GET['tier']) && $_GET['tier'] == $rows['tier']) { echo "selected"; } echo "
                                            >".$rows['tier']."</option>";
                                        }
                                    ?>
                                </select>
                            
                        </tr>
                        <tr>
                            <td colspan='2'>Submitted Date Range:</td>
                            <td>Keyword Search:</td>
                            <td>Applied To:</td>
                            <td>Authorized By:</td>
                            <td>Agent:</td>
                            <td>Status:</td>
                        </tr>
                        <tr>
                            <td>
                                <input style='width:95%;' type='date' name='submittedsdate'
                                        <?php if(isset($_GET['submittedsdate'])) {
                                            echo " value = '".$_GET['submittedsdate']."'";
                                        } ?>
                                />
                            </td>
                            <td>
                                <input type='date' name='submittededate'
                                    <?php if(isset($_GET['submittededate'])) {
                                            echo " value = '".$_GET['submittededate']."'";
                                    } ?>
                                /> 
                            </td>
                            <td>
                                <input type="text" name="user_notes" placeholder="Search term" style="width:100%;"
                                        <?php if(isset($_GET['user_notes'])) {
                                            echo "value='".$_GET['user_notes']."'";
                                        } ?>
                                />
                            </td>
                            <td>
                                <select name="appliedto" style="width: 100%;">
                                    <option value=""></option>
                                    <option value='Online' 
                                        <?php if(isset($_GET['appliedto']) && $_GET['appliedto'] == 'Online') {echo 'selected';} ?>
                                    >Online</option>
                                    <option value='Blended'
                                        <?php if(isset($_GET['appliedto']) && $_GET['appliedto'] == 'Blended') {echo 'selected';} ?>
                                    >Blended</option>
                                    <option value='Both'
                                    <?php if(isset($_GET['appliedto']) && $_GET['appliedto'] == 'Both') {echo 'selected';} ?>
                                    >Both</option>
                                </select>
                            </td>
                            <td>
                                <select name="authorizedby" id="authorizedby" style="width: 100%;">
                                        <option value=""></option>
                                        <?php
                                            include("../../live_connect/connect.inc");
                                            $query = mysqli_query($conn, "SELECT DISTINCT authorizedby FROM tickets_newtickets ORDER BY authorizedby ASC");
                                            while($rows = mysqli_fetch_array($query)) {
                                                if($rows['authorizedby'] != '') {
                                                $authorizedby = explode(".", $rows['authorizedby']);
                                                $authorizedby = ucwords(implode(" ", $authorizedby));
                                                echo "<option value='".$rows['authorizedby']."'";
                                                    if(isset($_GET['authorizedby']) && $_GET['authorizedby'] == $rows['authorizedby']) { echo "selected"; } echo "
                                                >".$authorizedby."</option>";
                                                }
                                            }
                                        ?>
                                </select>
                            </td>
                            <td>
                                <select name="agent" style="width: 100%;">
                                        <option value=""></option>
                                        <?php
                                            include("../../live_connect/connect.inc");
                                            $query = mysqli_query($conn, "SELECT DISTINCT agent FROM tickets_newtickets ORDER BY agent ASC");
                                            while($rows = mysqli_fetch_array($query)) {
                                                if($rows['agent'] != '') {
                                                $agent = explode(".", $rows['agent']);
                                                $agent = ucwords(implode(" ", $agent));
                                                echo "<option value='".$rows['agent']."'";
                                                    if(isset($_GET['agent']) && $_GET['agent'] == $rows['agent']) { echo "selected"; } echo "
                                                >".$agent."</option>";
                                                }
                                            }
                                        ?>
                                </select>
                            </td>
                            <td>
                                <select name="status" style="width: 100%;">
                                        <option value=""></option>
                                        <?php
                                            include("../../live_connect/connect.inc");
                                            $query = mysqli_query($conn, "SELECT DISTINCT status FROM tickets_newtickets ORDER BY status ASC");
                                            while($rows = mysqli_fetch_array($query)) {
                                                if($rows['status'] != '' || $rows['status'] != ' ') {
                                                $status = explode(".", $rows['status']);
                                                $status = ucwords(implode(" ", $status));
                                                echo "<option value='".$rows['status']."'";
                                                    if(isset($_GET['status']) && $_GET['status'] == $rows['status']) { 
                                                        echo "selected"; 
                                                    } 
                                                echo ">".$status."</option>";
                                                }
                                            }
                                        ?>
                                </select>
                            </td>
                          
                        </tr>
                        <tr>
                            <td>PV code</td>
                            <td colspan ='2'>Completed Date Range:</td>
                        </tr>
                        <tr>
                            <td>
                                <select name='PVcodes' style='width:100%;'>
                                    <option></option>
                                    <?php 
                                    include("../../live_connect/connect.inc");
                                    $query = mysqli_query($conn, "SELECT pvcode FROM PV ORDER BY pvcode ASC");
                                    while($row = mysqli_fetch_array($query)) {
                                        echo "<option value='".$row['pvcode']."'";
                                            if(isset($_GET['PVcodes']) && $_GET['PVcodes'] == $row['pvcode']) { 
                                                echo "selected"; 
                                            } 
                                        echo ">".$row['pvcode']."</option>";
                                    }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <input type='date' name='completedsdate'
                                    <?php if(isset($_GET['completedsdate'])) {
                                            echo " value = '".$_GET['completedsdate']."'";
                                    } ?>
                                /> 
                            </td>
                            <td>
                                <input type='date' name='completededate'
                                    <?php if(isset($_GET['completededate'])) {
                                            echo " value = '".$_GET['completededate']."'";
                                    } ?>
                                />
                            </td>
                        </tr>
                    </table>
                <input type="submit" value="Search"/>   
                </form>
        </div>
        
        <div id='container'>
        <div id='message'>
            <?php echo $message; ?>
        </div>
        <div id='search_btns'>
            <div class='quick_search' name='restart' id='restart' style='background-color: white; border: 3px solid black;'>
            <form id='resetform' method='GET'>
                <input type='hidden' value='restart' name='restart'/>
            </form>
                <p class = 'count' style='font-size: 20px; text-align: center;'>
                    RESET SEARCH
                </p>
                
            </div>
            <div class='quick_search' name='pending' id='pend' style='background-color: #dbcf1f'>
            <form id='pendform' method='GET'>
                <input type='hidden' value='subtest' name='subtest'/>
                <input type='hidden' value='Pending' name='status'/>
            </form>
                <p class = 'count'>
                    <?php
                        include("../../live_connect/connect.inc");
                        $pending_tickets_sql = "SELECT COUNT(*) FROM tickets_newtickets WHERE status = 'Pending'";
                        $pending_tickets_result = mysqli_query($conn, $pending_tickets_sql);
                        echo $pending_tickets_total = mysqli_fetch_array($pending_tickets_result)[0];
                    ?>
                </p>
                <p class = 'title'>
                    All Pending Tickets
                </p>
            </div>
            <div class='quick_search' name='pendingweek' id='pendweek' style='background-color: #dbcf1f'>
                    <?php
                        $tdate = date("Y-m-d");
                        $tdate2 = strtotime($tdate);
                        $edate = $tdate2 - 604800;
                        $edate = date("Y-m-d", $edate);
                    ?>
                <form id='pendweekform' method='GET'>
                    <input type='hidden' value='subtest' name='subtest'/>
                    <input type='hidden' value='Pending' name='status'/>
                    <input type='hidden' value='<?php echo $edate; ?>' name='submittedsdate'/>
                    <input type='hidden' value='<?php echo $tdate; ?>' name='submittededate'/>
                    
                </form>
                
                <p class='count'>
                    <?php
                        include("../../live_connect/connect.inc");
                        $weekpend_tickets_sql = "SELECT COUNT(*) FROM tickets_newtickets WHERE status = 'Pending' AND submitteddate BETWEEN '".$edate."' AND '".$tdate."'";
                        $weekpend_tickets_result = mysqli_query($conn, $weekpend_tickets_sql);
                        echo $weekpend_tickets_total = mysqli_fetch_array($weekpend_tickets_result)[0];
                    ?>
                </p>
                <p class='title'>
                    Pending This Week
                </p>                
            </div>
            <div class='quick_search' name='assigned' id='assn' style='background-color: Green'>
            <form id='assnform' method='GET'>
                <input type='hidden' value='subtest' name='subtest'/>
                <input type='hidden' value='Assigned' name='status'/>
            </form>
                <p class='count'>
                    <?php
                        include("../../live_connect/connect.inc");
                        $assigned_tickets_sql = "SELECT COUNT(*) FROM tickets_newtickets WHERE status = 'Assigned'";
                        $assigned_tickets_result = mysqli_query($conn, $assigned_tickets_sql);
                        echo $assigned_tickets_total = mysqli_fetch_array($assigned_tickets_result)[0];
                    ?>
                </p>
                <p class='title'>
                    All Assigned Tickets
                </p>                
            </div>
            <div class='quick_search' name='assignedweek' id='assnweek' style='background-color: Green'>
                    <?php
                        $tdate = date("Y-m-d");
                        $tdate2 = strtotime($tdate);
                        $edate = $tdate2 - 604800;
                        $edate = date("Y-m-d", $edate);
                    ?>
                    <form id='assnweekform' method='GET'>
                        <input type='hidden' value='subtest' name='subtest'/>
                        <input type='hidden' value='Assigned' name='status'/>
                        <input type='hidden' value='<?php echo $edate ?>' name='submittedsdate'/>
                        <input type='hidden' value='<?php echo $tdate ?>' name='submittededate'/>
                    </form>
                <p class='count'>
                    <?php
                        include("../../live_connect/connect.inc");
                        $assigned_tickets_sql = "SELECT COUNT(*) FROM tickets_newtickets WHERE status = 'Assigned' AND submitteddate BETWEEN '".$edate."' AND '".$tdate."'";
                        $assigned_tickets_result = mysqli_query($conn, $assigned_tickets_sql);
                        echo $assigned_tickets_total = mysqli_fetch_array($assigned_tickets_result)[0];
                    ?>
                </p>
                
                <p class='title'>
                    Assigned This Week
                </p>                
            </div>
            <div class='quick_search' name='duedate' id='duedate' style='background-color: Orange'>
                    <?php
                        $tdate = date("Y-m-d");
                    ?>
                    <form id='duedateform' method='GET'>
                        <input type='hidden' value='subtest' name='subtest'/>
                        <input type='hidden' value='<?php echo $tdate; ?>' name='duedate'/>
                    </form>
                <p class='count'>
                    <?php
                        include("../../live_connect/connect.inc");
                        $due_tickets_sql = "SELECT COUNT(*) FROM tickets_newtickets WHERE duedate < CURDATE() AND duedate != '0000-00-00' AND status != 'Completed' AND status != 'Rejected'";
                        $due_tickets_result = mysqli_query($conn, $due_tickets_sql);
                        echo $due_tickets_total = mysqli_fetch_array($due_tickets_result)[0];
                    ?>
                </p>
                
                <p class='title'>
                    Past Due Tickets
                </p>                
            </div>
            <div class='quick_search' name='compweek' id='compweek' style='background-color: Red'>
                <?php
                    $tdate = date("Y-m-d");
                    $tdate2 = strtotime($tdate);
                    $edate = $tdate2 - 604800;
                    $edate = date("Y-m-d", $edate);
                ?>
                <form id='compweekform' method='GET'>
                    <input type='hidden' value='subtest' name='subtest'/>
                    <input type='hidden' value='Completed' name='status'/>
                    <input type='hidden' value='<?php echo $edate ?>' name='completedsdate'/>
                    <input type='hidden' value='<?php echo $tdate ?>' name='completededate'/>
                </form>

                <p class='count'>
                    <?php
                        include("../../live_connect/connect.inc");
                        $assigned_tickets_sql = "SELECT COUNT(*) FROM tickets_newtickets WHERE status = 'Completed' AND completeddate BETWEEN '".$edate."' AND '".$tdate."'";
                        $assigned_tickets_result = mysqli_query($conn, $assigned_tickets_sql);
                        echo $assigned_tickets_total = mysqli_fetch_array($assigned_tickets_result)[0];
                    ?>
                </p>
                <p class='title'>
                    Completed This Week
                </p>
            </div>
            <div class='quick_search' name='compmonth' id='compmonth' style='background-color: Red'>
                <?php
                    $tdate = date("Y-m-d");
                    $tdate2 = strtotime($tdate);
                    
                    $edate = date("Y-m-d", strtotime("-1 month", $tdate2));
                ?>
                <form id='compmonthform' method='GET'>
                    <input type='hidden' value='subtest' name='subtest'/>
                    <input type='hidden' value='Completed' name='status'/>
                    <input type='hidden' value='<?php echo $edate ?>' name='completedsdate'/>
                    <input type='hidden' value='<?php echo $tdate ?>' name='completededate'/>
                </form>

                <p class='count'>
                    <?php
                        include("../../live_connect/connect.inc");
                        $assigned_tickets_sql = "SELECT COUNT(*) FROM tickets_newtickets WHERE status = 'Completed' AND completeddate BETWEEN '".$edate."' AND '".$tdate."'";
                        $assigned_tickets_result = mysqli_query($conn, $assigned_tickets_sql);
                        echo $assigned_tickets_total = mysqli_fetch_array($assigned_tickets_result)[0];
                    ?>
                </p>
                <p class='title'>
                    Completed This Month
                </p>                
            </div>
            </div>

            <div class='reports' style="padding-bottom: 5px;">
                <?php
                    $q = mysqli_query($conn, "SELECT * FROM tickets_admin WHERE username = '".$_COOKIE['un']."'");
                    while($r = mysqli_fetch_array($q)) {
                        if($r['max'] == 'x' ) {
                        echo '
                            <form action="ticketsearchcsvdl.php" method="GET" style="padding-bottom: 5px; ">
                                <input type="hidden" value="'.$sql.'" name="query"/>
                                <input type="submit" value="CSV Report">
                            </form>';
                        }
                        if($r['max'] == '' && $r['ADC'] == 'x') {
                        echo '
                            <form action="ADCcsvdl.php" method="GET">
                                <input type="hidden" value="'.$sql.'" name="query"/>
                                <input type="submit" value="ADC Report">
                            </form>
                        ';
                        }
                    }
                ?>
            </div>
        </div>

        <table cellspacing='0' cellpadding='0' class='iframe_table' id='table'>
            <tr>
                <th class='tdh' style='width:10px;'>ID</th>
                <th class='tdh'>Name</th>
                <th class='tdh'>Class</th>
                <th class='tdh'>Page</th>
                <th class='tdh'>Week</th>
                <th class='tdh'>Issue</th>
                <th class='tdh'>Submitted Date</th>
                <th class='tdh'>Expand</th>
            </tr>

        <?php
        
        
        include("../../live_connect/connect.inc");
        $query = mysqli_query($conn, $sql);
        while($rows = mysqli_fetch_array($query)) {
            if($rows['submitteddate'] == '0000-00-00') {
                $sdate = '';
            }
            else {
                $sdate = strtotime($rows['submitteddate']);
                $sdate = date('d-M-Y', $sdate);
            }
            if($rows['assigneddate'] == '0000-00-00') {
                $adate = '';
            }
            else {
                $adate = strtotime($rows['assigneddate']);
                $adate = date('d-M-Y',$adate);
            }
            if($rows['duedate'] == '0000-00-00') {
                $ddate = '';
            }
            else {
                $ddate = strtotime($rows['duedate']);
                $ddate = date('d-M-Y', $ddate);
            }
            if($rows['completeddate'] == '0000-00-00') {
                $cdate = '';
            }
            else {
                $cdate = strtotime($rows['completeddate']);
                $cdate = date('d-M-Y', $cdate);
            }
            echo "
                <tr>
                    <td class='tdc'>".$rows['id']."</td>
                    <td class='tdc'>";
                    $pos = strpos($rows['fullname'], '.');

                        if($pos > 0 || $pos != '') {
                            $name = explode('.', $rows['fullname']);
                        }
                        else {
                            $name = explode(' ', $rows['fullname']);
                        }
                    
                   echo ucfirst($name[0]) . " " .ucfirst($name[1])."</td> ";
                    if(strlen($rows['class']) > 6) {
                        $class = substr($rows['class'], 0, 6);
                    }
                    else {
                        $class = $rows['class'];
                    }
                    echo "
                    <td class='tdc'>".$class."</td>
                    <td class='tdc'>".$rows['page']."</td>
                    <td class='tdc'>".$rows['week']."</td>
                    <td class='tdc'>".$rows['issue']."</td>
                    <td class='tdc'>".$sdate."</td>
                    <td class='tdc' style='border-right: 0px;'>
                        <input type='checkbox' id='expand".$rows['id']."' class='expand' value='expand".$rows['id']."'/>
                            <label for='expand".$rows['id']."'>Expand</label>
                    </td>
                </tr>
                <tr class='dheader' id='adinfoheader".$rows['id']."'>
                    <td></td>
                    <td class='tdh'>Coure URL</td>
                    <td class='tdh' colspan='2'>Course Name</td>
                    <td class='tdh'>Program</td>
                    <td class='tdh'>Tier</td>
                    <td class='tdh'>Agent</td>
                    <td class='tdh'>Status</td>
                </tr>
                <tr class='details' id='adinfo".$rows['id']."'>
                    <td></td>
                    <td class='tdc' style='border-left: 1px;'>".$rows['courseurl']."</td>
                    <td class='tdc' colspan='2'>".$rows['cname']."</td>
                    <td class='tdc'>";
                    switch($rows['program']) {
                        case "AH":
                            echo "Healthcare";
                        break;
                        case "B":
                            echo "Business and Accounting";
                        break;
                        case "B/A":
                            echo "Business and Accounting";
                        break;
                        case "B?A":
                            echo "Business and Accounting";
                        break;
                        case "BIS":
                            echo "Business and Accounting";
                        break;
                        case "CS":
                            echo "Computer Science";
                        break;
                        case "DA":
                            echo "Graphic Arts";
                        break;
                        case "DES209":
                            echo "Graphic Arts";
                        break;
                        case "GA":
                            echo "Graphic Arts";
                        break;
                        case "GD":
                            echo "Graphic Arts";
                        break;
                        case "GE":
                            echo "General Education";
                        break;
                        case "GenEd":
                            echo "General Education";
                        break;
                        case " GW":
                            echo "Graphic Arts";
                        break;
                        case "HC":
                            echo "Healthcase";
                        break;
                        case "IT":
                            echo "Technology";
                        break;
                        case "RT":
                            echo "Respiratory Therapy";
                        break;
                        default:
                            echo $rows['program'];
                    }
                    echo "</td>
                    <td class='tdc'>".$rows['tier']."</td>
                    <td class='tdc'>";
                        $agent = explode(".", $rows['agent']);
                        $agent = implode(" ", $agent);
                        $agent = ucwords($agent);
                        echo $agent;
                    echo "</td>
                    <td class='tdc' style='border-bottom: 1px;'>".$rows['status']."</td>
                </tr>
                <tr id='dtheader".$rows['id']."' class='dheader'>
                    <td></td>
                    <td class='tdh' colspan='3'>Associated Degrees</td>
                    <td class='tdh'>Assigned Date</td>
                    <td class='tdh'>Due Date</td>
                    <td class='tdh' colspan='2'>Completed Date</td>
                </tr>
                <tr id='dt".$rows['id']."' class='details'>
                    <td></td>
                    <td class='tdc' colspan='3'>".$rows['degrees']."</td>
                    <td class='tdc'>".$adate."</td>
                    <td class='tdc'>".$ddate."</td>
                    <td class='tdc' colspan='2'>".$cdate."</td>
                </tr>
                <tr id='dheader".$rows['id']."' class='dheader'>
                    <td></td>
                    <td class='tdh' colspan='3'>Details</td>
                    <td class='tdh' colspan='4'>Final Notes</td>
                </tr>
                <tr id='detail".$rows['id']."' class='details' style='border-bottom: 1px;'>
                    <td class='tdc' style='border-top: 0px;'></td>
                    <td class='tdc' colspan='3'>".$rows['user_notes']."</td>
                    <td class='tdc' colspan='4'>".$rows['final_notes']."</td>
                </tr>
            ";
            }
            echo "</table>";    
                echo "<div class='table_btns'>  
                    <ul class='pagination'>
                        <li><a href='?'. $parms .'pageno=1'><<<</a></li>
                        <li class="; if($pageno <= 1){ echo 'disabled'; } echo ">
                            <a href='"; if($pageno <= 1){ echo '#'; } else { echo '?'. $parms .'pageno='.($pageno - 1); } echo "'><</a>
                        </li>
                        <li class='"; if($pageno >= $total_pages){ echo 'disabled'; } echo "'>
                            <a href='"; if($pageno >= $total_pages){ echo '#'; } else { echo '?'. $parms . 'pageno='.($pageno + 1); } echo "'>></a>
                        </li>
                        <li><a href='?"; if(isset($_GET['subtest'])) {echo $parms . "pageno=" . $total_pages;} else {echo 'pageno='.$total_pages;} echo "'>>>></a></li>
                    </ul>
                </div>";
        ?>
        </div>
    </content>
</body>
</html>

