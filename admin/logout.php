<?php
    session_start() ; 
    
    $un = "";
    $pw = "";
    $email = "";
    
    $cookie_name = 'un';
    $cookie_value = $un;
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
    
    $cookie_name = 'pw';
    $cookie_value = $pw;
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
    
    $cookie_name = 'email';
    $cookie_value = $email;
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day 
    
    session_destroy() ;
    
    header("Location: ../index.php");
?>

