<?php 
session_start([
    'cookie_lifetime' => 86400,
]); 
include("../includes/security.inc");
security("ADC");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page title</title>
    
    <link rel="stylesheet" href="../includes/styles.css">
</head>
<body id='main_page'>
	<nav class='menu'>
		<?php include_once('../includes/adminmenu.php') ?>
	</nav>
<content class="content" id="content">

<?php if(isset($_SESSION['msg'])) { echo $_SESSION['msg']; $_SESSION['msg'] = ""; } ?>

<div class="headingArea">
	ADC Report Page
</div>

<div class='page'>
<b>Selected Dates Report</b><p />
<form action='downloadrange.php' method='POST'>
	<input type='date' name='sdate'>
	<input type='date' name='edate'>
	<input type='submit' value='Run Report'>
</form>
	<p />
<b>All Records Report</b><p />
<form action='downloadall.php' method='POST'>
	<input type='submit' value='Run Report'>
</form>

</div>

</content>

</body>
</html>