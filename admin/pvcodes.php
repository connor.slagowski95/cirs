<?php 
session_start([
    'cookie_lifetime' => 86400,
]); 
include("../includes/security.inc");
security("max");
?>

<?php
if(isset($_POST['apv'])) {
	include("../../live_connect/connect.inc");
mysqli_query($conn, "INSERT INTO PV(pvcode, degree, program) VALUES('".$_POST['apv']."', '".$_POST['adegree']."', '".$_POST['aprogram']."')");
$_SESSION['msg'] = "<div class='headingArea'>Inserted Class Successfully</div>";
mysqli_close($conn);
}
?>

<?php
if(isset($_POST['upv'])) {
	include("../../live_connect/connect.inc");
	$statement = "UPDATE PV SET pvcode = '".$_POST['upv']."', degree = '".$_POST['udegree']."', program = '".$_POST['uprogram']."'  WHERE pvcode = '".$_POST['uoldpv']."'";
mysqli_query($conn, $statement) OR DIE('HAHA');
$_SESSION['msg'] = "<div class='headingArea'>Updated Class Successfully</div>";
mysqli_close($conn);
}
?>

<?php
if(isset($_POST['dpv'])) {
	include("../../live_connect/connect.inc");
mysqli_query($conn, "DELETE FROM PV WHERE pvcode = '".$_POST['dpv']."'");
$_SESSION['msg'] = "<div class='headingArea'>Deleted Class Successfully</div>";
mysqli_close($conn);
}
?>

<style>
.tab {
	border-collapse: collapse;
}
.tdh {
	font-weight: bold;
	border: 1px solid #000;
	text-align: left;
	padding: 5px;
}
.tdc {
	border: 1px solid #000;
	padding: 5px;
}
.tdcbutton {
	padding: 5px;
	font-weight: bold;
}
</style>

<!DOCTYPE html>
<html lang="en">


<head>
<link rel="stylesheet" href="../includes/styles.css">
</head>
<body id='main_page'>
    <nav class='menu'>
        <?php include_once('../includes/adminmenu.php') ?>
    </nav>

<content class="content" id="content">


<?php if(isset($_SESSION['msg'])) { echo $_SESSION['msg']; $_SESSION['msg'] = ""; } ?>

<div class="headingArea">
	PV Codes
</div>

<div class='page'>

<form action='pvcodes.php' method='POST'>
    <table class='tab'>
        <tr>
            <td class='tdh'>PV Code</td>
            <td class='tdh'>Degree Title</td>
            <td class='tdh'>Program</td>
            <td></td>
        </tr>
        <tr>
            <td class='tdc'><input type='text' name='apv' pattern="[A-Z0-9]{6,8}" required></td>
            <td class='tdc'><input type='text' name='adegree' required></td>
            <td class='tdc'><input type='text' name='aprogram' required></td>
            <td class='tdc'><input type='submit' class='tdcbutton' value='Add Degree'></td>
        </tr>
    </table>
</form>

<table class='tab'>
	<tr>
		<td class='tdh'>PV Code</td>
		<td></td>
	</tr>
<?php 
include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM PV ORDER BY pvcode ASC");
while($rows = mysqli_fetch_array($query)) {
	echo "<form id='form1' name='form1' action='pvcodes.php' method='POST'>
		<tr>
			<td class='tdc'>
				<input type='text' name='upv' value='" . $rows['pvcode'] . "' pattern='[A-Z0-9]{6,8}' required>
				<input type='hidden' name='uoldpv' value='" . $rows['pvcode'] . "'>
				<input type='text' name='udegree' value = '".$rows['degree']."' required>
				<input type='text' name='uprogram' value = '".$rows['program']."' required>
			</td>
			<td class='tdc'>
				<input type='submit' class='tdcbutton' value='Update'>
			</td>
        </form>
            <td class='tdc'>
                <form id='form2' name='form2' action='pvcodes.php' method='POST'>
                    <input type='hidden' name='dpv' value='".$rows['pvcode']."'>
                    <input type='submit' class='tdcbutton' value='Delete'>
                </form>
            </td>		
		</tr>
		";
}
mysqli_close($conn);
?>
</table>
</div>
</content>

</body>
</html>