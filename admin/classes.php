<?php 
session_start([
    'cookie_lifetime' => 86400,
]); 
include("../includes/security.inc");
security("max");
?>

<?php
if(isset($_POST['aclass'])) {
	include("../../live_connect/connect.inc");
mysqli_query($conn, "INSERT INTO tickets_newclasses(classId, className, pvcodes) VALUES('".$_POST['aclass']."', '".$_POST['acourse']."', '".$_POST['apv']."')");
$_SESSION['msg'] = "<div class='headingArea'>Inserted Class Successfully</div>";
mysqli_close($conn);
}
?>

<?php
if(isset($_POST['uclass'])) {
	include("../../live_connect/connect.inc");
	$statement = "UPDATE tickets_newclasses SET classId = '".$_POST['uclass']."', className = '".$_POST['ucname']."', pvcodes = '".$_POST['upv']."'  WHERE classId = '".$_POST['uoldclass']."'";
mysqli_query($conn, $statement) OR DIE('HAHA');
$_SESSION['msg'] = "<div class='headingArea'>Updated Class Successfully</div>";
mysqli_close($conn);
}
?>

<?php
if(isset($_POST['dclass'])) {
	include("../../live_connect/connect.inc");
mysqli_query($conn, "DELETE FROM tickets_newclasses WHERE classId = '".$_POST['dclass']."'");
$_SESSION['msg'] = "<div class='headingArea'>Deleted Class Successfully</div>";
mysqli_close($conn);
}
?>

<?php
	if (isset($_GET['pageno'])) {
		$pageno = $_GET['pageno'];
	}
	else {
		$pageno = 1;
	}

	if(isset($_POST['class']) && $_POST['class'] != '') {
		$no_of_records_per_page = 30;
	$offset = ($pageno-1) * $no_of_records_per_page; 
	
	include("../../live_connect/connect.inc");

		$total_pages_sql = "SELECT COUNT(*) FROM tickets_newclasses";
        $result = mysqli_query($conn,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);

		$sql = "SELECT * FROM tickets_newclasses WHERE classId = '".$_POST['class']."'";
	}
	else {

	$no_of_records_per_page = 30;
	$offset = ($pageno-1) * $no_of_records_per_page; 
	include("../../live_connect/connect.inc");

		$total_pages_sql = "SELECT COUNT(*) FROM tickets_newclasses";
        $result = mysqli_query($conn,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);

		$sql = "SELECT * FROM tickets_newclasses LIMIT $offset, $no_of_records_per_page";
	}
		
?>

<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="UTF-8">
<style>
.tab {
	border-collapse: collapse;
}
.tdh {
	font-weight: bold;
	border: 1px solid #000;
	text-align: left;
	padding: 5px;
}
.tdc {
	border: 1px solid #000;
	padding: 5px;
}
.tdcbutton {
	padding: 5px;
	font-weight: bold;
}
</style>
<link rel="stylesheet" href="../includes/styles.css">
</head>
<body id = 'main_page'>
    <nav class='menu'>
        <?php include_once('../includes/adminmenu.php') ?>
    </nav>

<content class="content" id="content">


<?php if(isset($_SESSION['msg'])) { echo $_SESSION['msg']; $_SESSION['msg'] = ""; } ?>

<div class="headingArea">
	Classes
</div>

<div class='page'>

When adding PV codes to the class the format is <span style='color:red;'>pvcode</span>,<span style='color:green;'>pvcode</span>.
There is no space between each code.<br /><br />

<form method='POST' style='display: inline;'>	
	Search By Class: 
	<select name="class" id="">
		<option value=""></option>
		<?php
		include("../../live_connect/connect.inc");
		$query = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
		while($rows = mysqli_fetch_array($query)) {
			echo "<option value='".$rows['classId']."'>".$rows['classId']."</option>";
		}
		?>
	</select>
	<input type='submit' value='Submit'/>
</form>
<form method='POST' style='display: inline;'>
    <input type='hidden' name='reset' value='reset'/>
    <input type='submit' value='Reset'/>
</form>
<br />
<br />
<form action='classes.php' method='POST'>
<table class='tab'>
	<tr>
		<td class='tdh'>Class ID</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>PV codes</td>
		<td></td>
	</tr>
	<tr>
		<td class='tdc'><input type='text' name='aclass' pattern="[A-Z0-9]{6,8}" required></td>
		<td class='tdc'><input type='text' name='acourse' required></td>
		<td class='tdc'><input type='text' name='apv' required></td>
		<td class='tdc'><input type='submit' class='tdcbutton' value='Add Class'></td>
	</tr>
</table>
</form>

<br />

<table class='tab'>
	<tr>
		<td class='tdh'>Class</td>
		<td></td>
	</tr>
<?php 
include("../../live_connect/connect.inc");
$query = mysqli_query($conn, $sql);
while($rows = mysqli_fetch_array($query)) {
	echo "
		<form id='form1' name='form1' action='classes.php' method='POST'>
			<tr>
				<td class='tdc'>
					<input type='text' name='uclass' value='" . $rows['classId'] . "' pattern='[A-Z0-9]{6,8}' required>
					<input type='hidden' name='uoldclass' value='" . $rows['classId'] . "'>
					<input type='text' name='ucname' value = '".$rows['className']."' required>
					<input type='text' name='upv' value = '".$rows['pvcodes']."' required>
				</td>
				<td class='tdc'>
					<input type='submit' class='tdcbutton' value='Update'>
				</td>
		</form>
				<td class='tdc'>
					<form id='form2' name='form2' action='classes.php' method='POST'>
						<input type='hidden' name='dclass' value='".$rows['classId']."'>
						<input type='submit' class='tdcbutton' value='Delete'>
					</form>
				</td>		
			</tr>
	";
}
mysqli_close($conn);
?>
</table>

<?php 
if($total_pages < 2) {
echo "
	<div class='table_btns'>  
		<ul class='pagination'>
			<li><a href='?pageno=1'><<<</a></li>
			<li class="; if($pageno <= 1){ echo 'disabled'; } echo ">
				<a href='"; if($pageno <= 1){ echo '#'; } else { echo '?pageno='.($pageno - 1); } echo "'><</a>
			</li>
			<li class='"; if($pageno >= $total_pages){ echo 'disabled'; } echo "'>
				<a href='"; if($pageno >= $total_pages){ echo '#'; } else { echo '?pageno='.($pageno + 1); } echo "'>></a>
			</li>
			<li><a href='?pageno=".$total_pages."'>>>></a></li>
		</ul>
	</div>
";
}?>

</div>
</content>

</body>
</html>