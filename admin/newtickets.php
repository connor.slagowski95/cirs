<?php 
session_start();
include("../includes/security.inc");
security("edit");

include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT COUNT(*) AS totaltier1 FROM tickets_newtickets WHERE tier = '1' AND status = 'Pending'");
while($rows = mysqli_fetch_array($query)) {
	$totaltier1 = $rows['totaltier1'];	
}
$query = mysqli_query($conn, "SELECT COUNT(*) AS totaltier2 FROM tickets_newtickets WHERE tier = '2' AND status = 'Pending'");
while($rows = mysqli_fetch_array($query)) {
	$totaltier2 = $rows['totaltier2'];	
}
$query = mysqli_query($conn, "SELECT COUNT(*) AS totaltier3 FROM tickets_newtickets WHERE tier = '3' AND status = 'Pending'");
while($rows = mysqli_fetch_array($query)) {
	$totaltier3 = $rows['totaltier3'];	
}

mysqli_close($conn);


if(isset($_POST['tid'])) {
$id = $_POST['tid'];
$agent = $_COOKIE['un'];
$datetaken = date("Y-m-d");
$duedate = date("Y-m-d", strtotime('+2 weeks', time()));
$status = "Assigned";

include("../../live_connect/connect.inc");
mysqli_query($conn, "INSERT INTO tickets_newticketlog(ticketid, usertaken, datetaken, duedate, status, editdate) 
	VALUES('".$id."', '".$agent."', '".$datetaken."', '".$duedate."', '".$status."', '".$datetaken."')");
	
mysqli_query($conn, "UPDATE tickets_newtickets SET status = 'Assigned', agent = '".$agent."', assigneddate = '".$datetaken."', duedate = '".$duedate."' WHERE id = '".$id."'");

mysqli_close($conn);

$_SESSION['msg'] = "<div class='headingArea'>Ticket Is Now Assigned To You</div>";
}

if(isset($_POST['temail'])) {
		$empname = $_COOKIE['un'];
		$empname = explode(".", $empname);
		$subname = $_POST['tname'];
		$to = $_POST['temail'];
		$tnum = $_POST['tid'];
		$subject = "Your Ticket: #".$tnum;
		$tdate = (date("d-M-Y"));
		$ddate = date("d-M-Y", strtotime('+2 weeks', time()));
		
		$msg = 
		"<html>
			<head>
				<title>HTML email</title>
			</head>
			<body>
				<h3> Hello Mr. /Ms. /Mrs. " . $subname . ", </h3>
				<p>My name is ".ucwords($empname[0])." and I have taken on your ticket that you submitted to the CDT. </p>
				<p>The current status of your ticket is <b style='color:#dbcf1f'>ASSIGNED</b>. </p>
				<p>This ticket was assigned today: ".$tdate." and we will have it done within two weeks or by this date: ".$ddate.".</p>
				<p>You will recieve additional emails as your ticket moves through our system.</p>
			</body>
		</html>";

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From: no-reply@iucdt.com";


		mail($to, $subject, $msg, $headers);
	}
?>

<style>

.tab {
	border-collapse: collapse;
	width: 100%;
}
.tab2 {
	margin-top: 15px;
	border-collapse: collapse;
	width: 800px;
	background-color: #FFF;
}
.tdh {
	font-weight: bold;
	border: 1px solid #000;
	text-align: left;
	padding: 5px;
	background-color: #f2f6f7;
}
.tdc {
	border: 1px solid #000;
	padding: 5px;
	background-color: #FFF;
}
.tdcbutton {
	padding: 5px;
	font-weight: bold;
	font-size: 12pt;
	
}
</style>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../includes/styles.css">

		<script>
			function toggle() {
				var x = document.getElementById("cont");
				if (x.style.display === "none") {
					x.style.display = "block";
				} else {
					x.style.display = "none";
				}
			} 
		</script>
    </head>
<body id='main_page'>
    <nav class='menu'>
        <?php include_once('../includes/adminmenu.php') ?>
    </nav>
<content class='content' id='content'>
<?php if(isset($_SESSION['msg'])) { echo $_SESSION['msg']; $_SESSION['msg'] = ""; } ?>

<div class="headingArea">
	New Tickets
</div>

<div class='page'>
<form action='newtickets.php' method='POST'>
<select name='tier'>
<option value="1" <?php if (isset($_POST['tier']) && $_POST['tier']=="1") echo ' selected';?>>Tier 1: <?php echo $totaltier1; ?> Tickets</option>
<option value="2" <?php if (isset($_POST['tier']) && $_POST['tier']=="2") echo ' selected';?>>Tier 2: <?php echo $totaltier2; ?> Tickets</option>
<option value="3" <?php if (isset($_POST['tier']) && $_POST['tier']=="3") echo ' selected';?>>Tier 3: <?php echo $totaltier3; ?> Tickets</option>
</select>
<input type='submit' value='Select'>
</form>



<?php 
if(isset($_POST['tier'])) {
	
include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE tier = '".$_POST['tier']."' AND status = 'Pending'");
while($rows = mysqli_fetch_array($query)) {
	
		$sdate = strtotime($rows['submitteddate']);
		$sdate = date('d-M-Y', $sdate);
	
	echo "
	<table class='tab' >
	<tr>
		<td class='tdh' colspan='2'>ID</td>
		<td class='tdh' colspan='2'>Fullname</td>
		<td class='tdh' colspan='3'>E-Mail</td>
		<td class='tdh' colspan='4'>Class</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2'>" . $rows['id'] . "</td>
		<td class='tdc' colspan='2'>" . $rows['fullname'] . "</td>
		<td class='tdc' colspan='3'>" . $rows['email'] . "</td>
		<td class='tdc' colspan='4'>" . $rows['class'] . "</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>Page</td>
		<td class='tdh' colspan='2'>Week</td>
		<td class='tdh' colspan='2'>Issue</td>
		<td class='tdh'>Tier</td>
		<td class='tdh' colspan='4'>Submitted Date</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2'>" . $rows['page'] . "</td>
		<td class='tdc' colspan='2'>" . $rows['week'] . "</td>
		<td class='tdc' colspan='2'>" . $rows['issue'] . "</td>
		<td class='tdc'>" . $rows['tier'] . "</td>
		<td class='tdc' colspan='4'>" . $sdate . "</td>
	</tr>
	<tr>
		<td class='tdc' colspan='11'><b>Note</b><br />" . $rows['user_notes'] . "</td>
	</tr>
	<tr>
		<td class='tdh' colspan='11' style='background-color: #ccc;'>
			<form action='newtickets.php' method='POST' style='float: right;'>
				<input type='hidden' name='tid' value='".$rows['id']."'>
				<input type='hidden' name='tname' value='".$rows['fullname']."'>
				<input type='hidden' name='temail' value='".$rows['email']."'>
				<input class='tdcbutton' type='submit' value='Take Ticket'>
			</form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
			<button id='toggle' OnClick='toggle();'>Hide/Show</button>
            <span id='cont' style='display:none;'>"; 
			$query2 = mysqli_query($conn, "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
		l.status AS status,	t.user_notes AS notes, t.completeddate AS datecomplete, l.duedate AS duedate, l.editdate AS editdate 
		FROM tickets_newtickets t, tickets_newticketlog l 
		WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'");
		echo "<table class='tab'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			if($r2['editdate'] == '0000-00-00') {
				$edate = '';
			}
			else {
				$edate = strtotime($r2['editdate']);
				$edate = date('d-M-Y', $edate);
			}
			if($r2['duedate'] == '0000-00-00') {
				$ddate = '';
			}
			else {
				$ddate = strtotime($r2['duedate']);
				$ddate = date('d-M-Y', $ddate);
			}
			if($r2['datetaken'] == '0000-00-00') {
				$tdate = '';
			}
			else {
				$tdate = strtotime($r2['datetaken']);
				$tdate = date('d-M-Y', $tdate);
			}
			
			echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $tdate . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $ddate . "</td>
				<td class='tdc'>" . $edate . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			"; 
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	</table>";
	
}
mysqli_close($conn);
echo "</div>";
}
?>

</div>
</content>
</body>
</html>