<?php 
session_start();
include("../includes/security.inc");
security("max");
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../includes/styles.css">
</head>

<body id='main_page'>
    <nav  class='menu'>
        <?php include_once('../includes/adminmenu.php') ?>
    </nav>
<div class="content" id='content'>

    <div class="headingArea">
    CIRS Data Output To CSV
    </div>

    <div class='page' style='font-size: 12pt;'>
        <form action='ticketscsvdl.php' method='POST'>
            Start Date: <input type='date' name='sdate'>
            End Date: <input type='date' name='edate'>
            <input type='submit' value='Run Report'>
        </form>
    </div>

</content>
</body>
</html>