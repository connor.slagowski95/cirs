<?php 
session_start();
include("../includes/security.inc");
security("ADC");

$agent = $_COOKIE['un'];
$editdate = date("Y-m-d");

if(isset($_POST['ustatus']) && $_POST['ustatus'] != '') {
include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE id = '".$_POST['uid']."'") OR DIE("HAHA");

while($row = mysqli_fetch_array($query)) {

	if($_POST['ustatus'] != $row['status']) {
		$empname = $_COOKIE['un'];
		$empname = explode(".", $empname);
		$subname = $_POST['ufullname'];
		$to = $_POST['uemail'];
		$tnum = $_POST['uid'];
		$subject = "Your Ticket: #".$tnum;
		$note = $_POST['ufinalnotes'];
		
		if ($_POST['ustatus'] == 'Pending') {

			$msg = 
		"<html>
			<head>
				<title>HTML email</title>
			</head>
			<body>
				<h3> Hello Mr. /Ms. /Mrs. " . $subname . ", </h3>
				<p>Your ticket has been set back to the <b style='color:orange'>".strtoupper($_POST['ustatus'])."</b> status. </p>

				<p>It will be reassigned to a new CDT member when we get the chance to get back to it. Thank you for your patience, in the mean time.</p>
				<p>You will recieve additional emails as your ticket moves through our system. </p>
			</body>
		</html>";
		} 
		
		if ($_POST['ustatus'] == 'Completed') {
			$color = "color: green";

			$msg = 
		"<html>
			<head>
				<title>HTML email</title>
			</head>
			<body>
				<h3> Hello Mr. /Ms. /Mrs. " . $subname . ", </h3>
				<p>We thank you for your submission to the CDT and we are happy to say that your ticket number ".$tnum." has been taken care
				of. </p>
				<p>The current status of your ticket is <b style='color:green'>".strtoupper($_POST['ustatus'])."
				</b>.</p>
				<p>We hope that you have a wonderful day.</p>
			</body>
		</html>";
		}
		if ($_POST['ustatus'] == 'Rejected') {

			$msg = 
		"<html>
			<head>
				<title>HTML email</title>
			</head>
			<body>
				<h3> Hello Mr. /Ms. /Mrs. " . $subname . ", </h3>
				<p>We have reviewed the ticket that you submitted to the CDT and your request has been denied. The reasoning for this
				decision is recorded below for your benefit.</p>
				<p>The current status of your ticket is <b style='color:red'>".strtoupper($_POST['ustatus'])."
				</b></p>
				<p>The reason for the rejection: <b>".$note."</b></p>
				
			</body>
		</html>";
		}

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From: no-reply@iucdt.com";


		mail($to, $subject, $msg, $headers);
	}
}
if(isset($_POST['ustatus']) && $_POST['ustatus'] != '') {
		include("../../live_connect/connect.inc");

		$query = mysqli_query($conn, "SELECT * FROM tickets_newclasses WHERE classId = '".$_POST['uclass']."'");
		while($rows = mysqli_fetch_array($query)) {
			$ucname = $rows['className'];
			$upv = $rows['pvcodes'];
		}

		$upv = explode(",", $upv);

		foreach($upv as $pvcode) {
			$query = mysqli_query($conn, "SELECT * FROM PV WHERE pvcode = '".$pvcode."'");
			while($rows = mysqli_fetch_array($query)) {
				$program = $rows['program'];
			}
		}

		$q = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE id = '".$_POST['uid']."'");
		while($r = mysqli_fetch_array($q)) {
			$cdtnotes = $r['CDT_notes'];
			$datetaken = $r['assigneddate'];
			$duedate = $r['duedate'];
		}

		mysqli_query($conn, "UPDATE tickets_newtickets SET 
		class = '".$_POST['uclass']."',
		cname = '".$ucname."',
		program = '".$program."',
		CDT_notes = '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."',
		final_notes = '".htmlspecialchars($_POST['ufinalnotes'], ENT_QUOTES)."',
		appliedto = '".$_POST['uapplied']."',
		authorizedby = '".$_POST['uauth']."'
		WHERE id = '".$_POST['uid']."'");
		mysqli_close($conn);
	if($_POST['ustatus'] == 'Assigned') {
		$status = "Edited Ticket Information";
		
		include("../../live_connect/connect.inc");
		mysqli_query($conn, "UPDATE tickets_newtickets SET CDT_notes = '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."',
			final_notes = '".htmlspecialchars($_POST['ufinalnotes'], ENT_QUOTES)."' WHERE id = '".$_POST['uid']."'");
		mysqli_query($conn, "INSERT INTO tickets_newticketlog(ticketid, usertaken, datetaken, duedate, status, notes, editdate) 
			VALUES('".$_POST['uid']."', '".$agent."', '".$datetaken."', '".$duedate."', '".$status."', '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."', '".$editdate."')");
		mysqli_close($conn);
		$_SESSION['msg'] = "<div class='headingArea'>Updated Note Successfully</div>";
	}
	elseif($_POST['ustatus'] == 'Completed') {
		include("../../live_connect/connect.inc");
		$q = mysqli_query($conn, "SELECT completeddate FROM tickets_newtickets WHERE id = '".$_POST['uid']."'");
		$completeddate = '0000-00-00';
		while($r = mysqli_fetch_array($q)) {
			$completeddate = $r['completeddate'];
		}
		if($completeddate=='0000-00-00') {
			$completeddate = date("Y-m-d");
		}
		$status = "Completed";
		mysqli_query($conn, "UPDATE tickets_newtickets SET CDT_notes = '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."', status = '".$status."', completeddate = '".$completeddate."' WHERE id = '".$_POST['uid']."'");
		mysqli_query($conn, "INSERT INTO tickets_newticketlog(ticketid, usertaken, status, notes, datecompleted, editdate) 
			VALUES('".$_POST['uid']."', '".$agent."', '".$status."', '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."', '".$completeddate."', '".$editdate."')");
		mysqli_close($conn);
		$_SESSION['msg'] = "<div class='headingArea'>Ticket Status Changed to Completed</div>";
	}
	elseif($_POST['ustatus'] == 'Rejected') {
		$status = "Rejected";
		$completeddate = date("Y-m-d");
		include("../../live_connect/connect.inc");
		mysqli_query($conn, "UPDATE tickets_newtickets SET CDT_notes = '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."', status = '".$status."', completeddate = '".$completeddate."' WHERE id = '".$_POST['uid']."'");
		mysqli_query($conn, "INSERT INTO tickets_newticketlog(ticketid, usertaken, status, notes, datecompleted, editdate) 
			VALUES('".$_POST['uid']."', '".$agent."', '".$status."', '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."', '".$completeddate."', '".$editdate."')");
		mysqli_close($conn);
		$_SESSION['msg'] = "<div class='headingArea'>Ticket Status Changed to Rejected and Closed</div>";
	}
	elseif($_POST['ustatus'] == 'Pending') {
		$status = "Pending";
		$editdate = date("Y-m-d");
		include("../../live_connect/connect.inc");
		mysqli_query($conn, "UPDATE tickets_newtickets SET agent = '', CDT_notes = '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."', status = '".$status."' WHERE id = '".$_POST['uid']."'");
		mysqli_query($conn, "INSERT INTO tickets_newticketlog(ticketid, usertaken, status, notes, editdate) 
			VALUES('".$_POST['uid']."', '".$agent."', '".$status."', '".htmlspecialchars($_POST['ucdtnotes'], ENT_QUOTES)."', '".$editdate."')");
		mysqli_close($conn);
		$_SESSION['msg'] = "<div class='headingArea'>Ticket Status Changed to Pending</div>";
	}
}
}

?>

<style>
        table {
            border: 1px solid lightgrey;
			border-collapse: collapse;
		}
		.tab {
			border-collapse: collapse;
			width: 100%;
		}
        .tdh {
            text-align:left;
            color: #FFFFFF;
            background-color: #013245;
            border: none;
            padding-left: 5px;
        }
        .tdc {
            border: solid lightgray;
            border-width: 1px 1px 0px 0px;
            padding: 5px 5px;
        }
        .dheader {
            display: none;

        }
        .details {
            display: none;
        }
    </style>
<!DOCTYPE html>
<html lang="en">
	<head>
		
		<script>
		<?php
			include("../../live_connect/connect.inc");
			$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE agent = '".$agent."' AND status = 'Assigned' ORDER BY submitteddate ASC");
			while($row = mysqli_fetch_array($query)) {
		?>
			function toggle<?php echo $row['id'] ?>() {
				
				var x<?php echo $row['id'] ?> = document.getElementById("cont<?php echo $row['id'] ?>");
				if (x<?php echo $row['id'] ?>.style.display === "none") {
					x<?php echo $row['id'] ?>.style.display = "block";
				} else {
					x<?php echo $row['id'] ?>.style.display = "none";
				
			} 
		}
			<?php } ?>
		</script>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../includes/styles.css">
	</head>
<body id='main_page'>
    <nav class='menu'>
        <?php include_once('../includes/adminmenu.php') ?>
    </nav>
    <content id='content' class='content'>
<?php if(isset($_SESSION['msg'])) { echo $_SESSION['msg']; $_SESSION['msg'] = ""; } ?>

<div class="headingArea">
	My Tickets
</div>

<div class='page'>
<b>Sorting Feature</b><p />
<form action='mytickets.php' method='POST'>
	<select name='colsort'>
	<?php echo "
	<option value='id' "; if(isset($_POST['colsort']) && $_POST['colsort']=='id') { echo " selected"; } echo ">ID</option>
	<option value='fullname' "; if(isset($_POST['colsort']) && $_POST['colsort']=='fullname') { echo " selected"; } echo ">Fullname</option>
	<option value='email' "; if(isset($_POST['colsort']) && $_POST['colsort']=='email') { echo " selected"; } echo ">E-Mail</option>
	<option value='courseurl' "; if(isset($_POST['colsort']) && $_POST['colsort']=='courseurl') { echo " selected"; } echo ">Course URL</option>
	<option value='class' "; if(isset($_POST['colsort']) && $_POST['colsort']=='class') { echo " selected"; } echo ">Class</option>
	<option value='page' "; if(isset($_POST['colsort']) && $_POST['colsort']=='page') { echo " selected"; } echo ">Page</option>
	<option value='week' "; if(isset($_POST['colsort']) && $_POST['colsort']=='week') { echo " selected"; } echo ">Week</option>
	<option value='reason' "; if(isset($_POST['colsort']) && $_POST['colsort']=='reason') { echo " selected"; } echo ">Reason</option>
	<option value='tier' "; if(isset($_POST['colsort']) && $_POST['colsort']=='tier') { echo " selected"; } echo ">Tier</option>
	<option value='priority' "; if(isset($_POST['colsort']) && $_POST['colsort']=='priority') { echo " selected"; } echo ">Priority</option>
	<option value='submitteddate' "; if(isset($_POST['colsort']) && $_POST['colsort']=='submitteddate') { echo " selected"; } echo ">Submitted Date</option>
	<option value='status' "; if(isset($_POST['colsort']) && $_POST['colsort']=='status') { echo " selected"; } echo ">Status</option>
	"; ?>
	</select>

	<select name='adsort'>
	<?php
		echo "
		<option value='ASC' "; if(isset($_POST['adsort']) && $_POST['adsort']=='ASC') { echo " selected"; } echo ">Ascending</option>
		<option value='DESC' "; if(isset($_POST['adsort']) && $_POST['adsort']=='DESC') { echo " selected"; } echo ">Descending</option>
	"; ?>
	</select>
	
	<input type='submit' value='Sort'>
</form>
	<p />

<?php	
if(isset($_POST['adsort']) && $_POST['adsort'] != '') {
	$colsort = $_POST['colsort'];
	$adsort = $_POST['adsort'];
	echo "<table class='tab'>";
	include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE agent = '".$agent."' AND status = 'Assigned' ORDER BY ".$colsort." ".$adsort."") OR DIE("HAHA");
while($rows = mysqli_fetch_array($query)) {
	if($rows['submitteddate'] == '0000-00-00') {
		$sdate = '';
	}
	else {
		$sdate = strtotime($rows['submitteddate']);
		$sdate = date('d-M-Y', $sdate);
	}
	if($rows['assigneddate'] == '0000-00-00') {
		$adate = '';
	}
	else {
		$adate = strtotime($rows['assigneddate']);
		$adate = date('d-M-Y',$adate);
	}
	if($rows['duedate'] == '0000-00-00') {
		$ddate = '';
	}
	else {
		$ddate = strtotime($rows['duedate']);
		$ddate = date('d-M-Y', $ddate);
	}
	if($rows['completeddate'] == '0000-00-00') {
		$cdate = '';
	}
	else {
		$cdate = strtotime($rows['completeddate']);
		$cdate = date('d-M-Y', $ddate);
	}
	echo "<form action='mytickets.php' method='POST'>
	<tr>
		<td class='tdh'>ID</td>
		<td class='tdh'>Fullname</td>
		<td class='tdh'>E-Mail</td>
		<td class='tdh'>Course URL</td>
		<td class='tdh'>Class</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>Page</td>
	</tr>
	<tr>
			<td class='tdc'>" . $rows['id'] . "<input type='hidden' name='uid' value='".$rows['id']."'></td>
			<td class='tdc'>" . $rows['fullname'] . "<input type='hidden' name='ufullname' value='".$rows['fullname']."'></td>
			<td class='tdc'>" . $rows['email'] . "<input type='hidden' name='uemail' value='".$rows['email']."'></td>
			<td class='tdc'><a href='" . $rows['courseurl'] . "' target='_blank'>URL Link</a><input type='hidden' name='ucourseurl' value='".$rows['courseurl']."'></td>
			<td class='tdc'>
			<select name='uclass'>";
				$q = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
				while($r = mysqli_fetch_array($q)) {
					echo "<option "; if ($r['classId'] == $rows['class']) { echo " selected"; } echo ">".$r['classId']."</option>";
				}
			echo "</select></td>
			<td class='tdc'>";
				$q = mysqli_query($conn, "SELECT * FROM tickets_newclasses");

				while($r = mysqli_fetch_array($q)) {
					if($r['classId'] == $rows['class']) {
						echo $r['className'];
						echo "<input type='hidden' name='cname' value='".$r['className']."'/>";
					}
				}
			echo"
			</td>
			<td class='tdc'>" . $rows['page'] . "<input type='hidden' name='upage' value='".$rows['page']."'></td>
	</tr>
	<tr>
		<td class='tdh'>Program</td>
		<td class='tdh'>Week</td>
		<td class='tdh'>Issue</td>
		<td class='tdh'>Tier</td>
		<td class='tdh'>Priority</td>
		<td class='tdh'>Submitted Date</td>
		<td class='tdh'>Status</td>
	</tr>
	<tr>
			<td class='tdc'>
				<select name='uprogram'>
					<option></option>";
							$query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms") OR DIE("HAHA");

							while($row2 = mysqli_fetch_array($query2)) {
								echo "<option value='".$row2['program']."'"; if($rows['program'] == $row2['program']) {echo 'selected';} echo "> ".$row2['program']."</option>";
							}
					echo "
				</select>
			</td>
			<td class='tdc'>" . $rows['week'] . "<input type='hidden' name='uweek' value='".$rows['week']."'></td>
			<td class='tdc'>" . $rows['issue'] . "<input type='hidden' name='uissue' value='".$rows['issue']."'></td>
			<td class='tdc'>" . $rows['tier'] . "<input type='hidden' name='utier' value='".$rows['tier']."'></td>
			<td class='tdc'>" . $rows['priority'] . "<input type='hidden' name='upriority' value='".$rows['priority']."'></td>
			<td class='tdc'>" . $sdate . "<input type='hidden' name='usubmitteddate' value='".$rows['submitteddate']."'></td>
			<td class='tdc'>
				<select name='ustatus'>
				<option value='Assigned' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Assigned') { echo " selected"; } echo ">Assigned</option>
				<option value='Completed' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Completed') { echo " selected"; } echo ">Completed</option>
				<option value='Pending' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Pending') { echo " selected"; } echo ">Pending</option>
				<option value='Rejected' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Rejected') { echo " selected"; } echo ">Rejected</option>
				</select></td>
		</tr>
	<tr>
		<td class='tdh'>Apply Change To</td>
		<td class='tdh'>Authorized by</td>
		<td class='tdh'>File 1</td>
		<td class='tdh'>File 2</td>
		<td class='tdh'>File 3</td>
		<td class='tdh'>Assigned Date</td>
		<td class='tdh'>Due Date</td>
	</tr>
	<tr>
		<td class='tdc'><select name='uapplied' style='width: 100%;'>
			<option></option>
			<option"; if($rows['appliedto'] == 'Online') { echo " selected"; } echo ">Online</option>
			<option"; if($rows['appliedto'] == 'Blended') { echo " selected"; } echo ">Blended</option>
			<option"; if($rows['appliedto'] == 'Both') { echo " selected"; } echo ">Both</option>
			</select>
		</td>
		<td class='tdc'>
				<select name='uauth'>
					<option></option>";
					$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
					while($r = mysqli_fetch_array($q)) {
						$user = explode(".", $r['username']);
						$user = implode(" ", $user);
						$user = ucwords($user);
						echo "<option "; if ($r['username'] == $rows['authorizedby']) { echo " selected"; } echo ">".$user."</option>";
					}
				echo "</select></td>
		<td class='tdc'>"; if($rows['file1'] != '') echo "<a href='../uploads/".$rows['file1']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file2'] != '') echo "<a href='../uploads/".$rows['file2']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file3'] != '') echo "<a href='../uploads/".$rows['file3']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>".$adate."</td>";
		echo "<td class='tdc'>".$ddate."</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>User Notes</td>
		<td class='tdh' colspan='3'>CDT Notes</td>
		<td class='tdh' colspan='2'>Final Notes</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2'>" . $rows['user_notes'] . "</td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ucdtnotes'>".$rows['CDT_notes']."</textarea></td>
		<td class='tdc' colspan='2'><textarea style=' width: 100%;height: 250px; resize: none;' name='ufinalnotes'>" . $rows['final_notes'] . "</textarea></td>
	</tr>
	<tr style='background-color: #ccc;'>
		<td class='tdc' colspan='12' style='text-align: left;'>";
		
		echo "<input class='tdcbutton' type='submit' value='Update Ticket' style='float: right;'></form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
            <button id='toggle' OnClick='toggle".$r1['ticketid']."();'>Hide/Show</button>
            <span id='cont".$r1['ticketid']."' style='display:none;'>"; 
			$query2 = mysqli_query($conn, "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
			l.status AS status,	l.notes AS notes, t.completeddate AS datecomplete, l.duedate AS duedate, l.editdate AS editdate 
			FROM tickets_newtickets t, tickets_newticketlog l 
			WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'");
		echo "<table class='tab2'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			if($r2['datetaken'] == '0000-00-00') {
				$tdate = '';
			}
			else {
				$tdate = strtotime($r2['datetaken']);
				$tdate = date('d-M-Y', $tdate);
			}
			if($r2['duedate'] == '0000-00-00') {
				$ddate = '';
			}
			else {
				$ddate = strtotime($r2['duedate']);
				$ddate = date('d-M-Y', $ddate);
			}
			if($r2['editdate'] == '0000-00-00') {
				$edate = '';
			}
			else {
				$edate = strtotime($r2['editdate']);
				$edate = date('d-M-Y', $edate);
			}
			
				echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $tdate . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $ddate . "</td>
				<td class='tdc'>" . $edate . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			"; 
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	";
}
mysqli_close($conn);
echo "</table>";
}
else {
echo "<table class='tab'>";
include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM tickets_newtickets WHERE agent = '".$agent."' AND status = 'Assigned' ORDER BY submitteddate ASC");
while($rows = mysqli_fetch_array($query)) {
	if($rows['submitteddate'] == '0000-00-00') {
		$sdate = '';
	}
	else {
		$sdate = strtotime($rows['submitteddate']);
		$sdate = date('d-M-Y', $sdate);
	}
	if($rows['assigneddate'] == '0000-00-00') {
		$adate = '';
	}
	else {
		$adate = strtotime($rows['assigneddate']);
		$adate = date('d-M-Y',$adate);
	}
	if($rows['duedate'] == '0000-00-00') {
		$ddate = '';
	}
	else {
		$ddate = strtotime($rows['duedate']);
		$ddate = date('d-M-Y', $ddate);
	}
	if($rows['completeddate'] == '0000-00-00') {
		$cdate = '';
	}
	else {
		$cdate = strtotime($rows['completeddate']);
		$cdate = date('d-M-Y', $ddate);
	}
	echo "<form action='mytickets.php' method='POST'>
	<tr>
		<td class='tdh'>ID</td>
		<td class='tdh'>Fullname</td>
		<td class='tdh'>E-Mail</td>
		<td class='tdh'>Course URL</td>
		<td class='tdh'>Class</td>
		<td class='tdh'>Course Name</td>
		<td class='tdh'>Page</td>
	</tr>
	<tr>
			<td class='tdc'>" . $rows['id'] . "<input type='hidden' name='uid' value='".$rows['id']."'></td>
			<td class='tdc'>" . $rows['fullname'] . "<input type='hidden' name='ufullname' value='".$rows['fullname']."'></td>
			<td class='tdc'>" . $rows['email'] . "<input type='hidden' name='uemail' value='".$rows['email']."'></td>
			<td class='tdc'><a href='" . $rows['courseurl'] . "' target='_blank'>URL Link</a><input type='hidden' name='ucourseurl' value='".$rows['courseurl']."'></td>
			<td class='tdc'>
			<select name='uclass'>";
				$q = mysqli_query($conn, "SELECT classId FROM tickets_newclasses ORDER BY classId ASC");
				while($r = mysqli_fetch_array($q)) {
					echo "<option "; if ($r['classId'] == $rows['class']) { echo " selected"; } echo ">".$r['classId']."</option>";
				}
				echo "</select></td>
			<td class='tdc'>".$rows['cname']."</td>
			<td class='tdc'>" . $rows['page'] . "<input type='hidden' name='upage' value='".$rows['page']."'></td>
	</tr>
	<tr>
		<td class='tdh'>Program</td>
		<td class='tdh'>Week</td>
		<td class='tdh'>Issue</td>
		<td class='tdh'>Tier</td>
		<td class='tdh'>Priority</td>
		<td class='tdh'>Submitted Date</td>
		<td class='tdh'>Status</td>
	</tr>
	<tr>
		<td class='tdc'>".$rows['program']."</td>
			<td class='tdc'>" . $rows['week'] . "<input type='hidden' name='uweek' value='".$rows['week']."'></td>
			<td class='tdc'>" . $rows['issue'] . "<input type='hidden' name='uissue' value='".$rows['issue']."'></td>
			<td class='tdc'>" . $rows['tier'] . "<input type='hidden' name='utier' value='".$rows['tier']."'></td>
			<td class='tdc'>" . $rows['priority'] . "<input type='hidden' name='upriority' value='".$rows['priority']."'></td>
			<td class='tdc'>" . $sdate . "<input type='hidden' name='usubmitteddate' value='".$rows['submitteddate']."'></td>
			<td class='tdc'>
				<select name='ustatus'>
				<option value='Assigned' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Assigned') { echo " selected"; } echo ">Assigned</option>
				<option value='Completed' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Completed') { echo " selected"; } echo ">Completed</option>
				<option value='Pending' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Pending') { echo " selected"; } echo ">Pending</option>
				<option value='Rejected' "; if(isset($_POST['searchstatus']) && $_POST['searchstatus']=='Rejected') { echo " selected"; } echo ">Rejected</option>
				</select></td>
		</tr>
	<tr>
		<td class='tdh'>Apply Change To</td>
		<td class='tdh'>Authorized by</td>
		<td class='tdh'>File 1</td>
		<td class='tdh'>File 2</td>
		<td class='tdh'>File 3</td>
		<td class='tdh'>Assigned Date</td>
		<td class='tdh'>Due Date</td>
	</tr>
	<tr>
		<td class='tdc'><select name='uapplied' style='width: 100%;'>
			<option></option>
			<option"; if($rows['appliedto'] == 'Online') { echo " selected"; } echo ">Online</option>
			<option"; if($rows['appliedto'] == 'Blended') { echo " selected"; } echo ">Blended</option>
			<option"; if($rows['appliedto'] == 'Both') { echo " selected"; } echo ">Both</option>
			</select>
		</td>
		<td class='tdc'>
				<select name='uauth'>
					<option></option>";
					$q = mysqli_query($conn, "SELECT username FROM tickets_admin ORDER BY username ASC");
					while($r = mysqli_fetch_array($q)) {
						$user = explode(".", $r['username']);
						$user = implode(" ", $user);
						$user = ucwords($user);
						echo "<option  value='".$r['username']."' "; if ($r['username'] == $rows['authorizedby']) { echo " selected"; } echo ">".$user."</option>";
					}
				echo "</select></td>
		<td class='tdc'>"; if($rows['file1'] != '') echo "<a href='../uploads/".$rows['file1']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file2'] != '') echo "<a href='../uploads/".$rows['file2']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>"; if($rows['file3'] != '') echo "<a href='../uploads/".$rows['file3']."' target='_blank'>View/Download File</a>";
		echo "<td class='tdc'>".$adate."</td>";
		echo "<td class='tdc'>".$ddate."</td>
	</tr>
	<tr>
		<td class='tdh' colspan='2'>User Notes</td>
		<td class='tdh' colspan='3'>CDT Notes</td>
		<td class='tdh' colspan='2'>Final Notes</td>
	</tr>
	<tr>
		<td class='tdc' colspan='2' valign='top'>" . $rows['user_notes'] . "</td>
		<td class='tdc' colspan='3'><textarea style=' width: 100%;height: 250px; resize: none;' name='ucdtnotes'>".$rows['CDT_notes']."</textarea></td>
		<td class='tdc' colspan='2'><textarea style=' width: 100%;height: 250px; resize: none;' name='ufinalnotes'>" . $rows['final_notes'] . "</textarea></td>
	</tr>
	<tr style='background-color: #ccc;'>
		<td class='tdc' colspan='12' style='text-align: left;'>";
		
		echo "<input class='tdcbutton' type='submit' value='Update Ticket' style='float: right;'></form>";
			
	$q = mysqli_query($conn, "SELECT ticketid FROM tickets_newticketlog WHERE ticketid = '".$rows['id']."' LIMIT 1");
	while($r1 = mysqli_fetch_array($q)) {
		echo "<span>
		<b>Ticket History:</b> 
		<button id='toggle' OnClick='toggle".$r1['ticketid']."();'>Hide/Show</button>
			<span id='cont".$r1['ticketid']."' style='display:none;'>"; 
			$statement = "SELECT t.id, l.ticketid, l.usertaken AS usertaken, l.datetaken AS datetaken, 
			l.status AS status,	l.notes AS notes, t.completeddate AS datecomplete, l.duedate AS duedate, l.editdate AS editdate 
			FROM tickets_newtickets t, tickets_newticketlog l 
			WHERE t.id = l.ticketid AND l.ticketid = '".$rows['id']."'";
			$query2 = mysqli_query($conn, $statement);
		echo "<table class='tab'>";
			
		while($r2 = mysqli_fetch_array($query2)) {
			if($r2['datetaken'] == '0000-00-00') {
				$tdate = '';
			}
			else {
				$tdate = strtotime($r2['datetaken']);
				$tdate = date('d-M-Y', $tdate);
			}
			if($r2['duedate'] == '0000-00-00') {
				$ddate = '';
			}
			else {
				$ddate = strtotime($r2['duedate']);
				$ddate = date('d-M-Y', $ddate);
			}
			if($r2['editdate'] == '0000-00-00') {
				$edate = '';
			}
			else {
				$edate = strtotime($r2['editdate']);
				$edate = date('d-M-Y', $edate);
			}
			
				echo "<tr>
				<td class='tdh'>User Taken</td>
				<td class='tdh'>Date Taken</td>
				<td class='tdh'>Status</td>
				<td class='tdh'>Due Date</td>
				<td class='tdh'>Edited Date</td>
			</tr>
			<tr>
				<td class='tdc'>" . $r2['usertaken'] . "</td>
				<td class='tdc'>" . $tdate . "</td> 
				<td class='tdc'>" . $r2['status'] . "</td>
				<td class='tdc'>" . $ddate . "</td>
				<td class='tdc'>" . $edate . "</td>
			</tr> 
			<tr>
				<td colspan='5' class='tdh'>CDT Notes</td>
			</tr>
			<tr>
				<td colspan='5' class='tdc'>" . $r2['notes'] . "</td>
			</tr>
			<tr>
				<td colspan='5' style='height: 15px; background-color: #ccc;'></td>
			</tr>
			"; 
		}
			echo "</table></span>
       </span>";
	}
		echo "
	   </td>
	</tr>
	<tr>
		<td colspan='11' style='height: 15px;'></td>
	</tr>
	";
}
mysqli_close($conn);
echo "</table>";
}
?>


</div>
</content>

</body>
</html>