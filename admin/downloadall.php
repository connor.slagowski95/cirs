<?php 
session_start();
// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header("Content-Disposition: attachment; filename='adcall.csv'");

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('Student ID', 'Full Name', 'Email', 'Course URL', 'Class', 'Page', 'Week', 'Issue', 'Tier', 'Priority', 'Submitted Date', 'Status'));

// fetch the data
include("../../live_connect/connect.inc");
	$query2 = mysqli_query($conn, "SELECT * FROM tickets_newtickets ORDER BY submitteddate DESC");
	while($row2 = mysqli_fetch_array($query2)) {
		fputcsv($output, array($row2['id'], $row2['fullname'], $row2['email'], $row2['courseurl'], $row2['class'], $row2['page'], $row2['week'], $row2['issue'], $row2['tier'], $row2['priority'], $row2['submitteddate'], $row2['status']));
	}
?>