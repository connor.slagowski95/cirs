<?php 
session_start();
if(isset($_POST['query'])) {
    $tdate = date("Y-m-d H:i:s");

    // output headers so that the file is downloaded rather than displayed
    header('Content-Type: text/csv; charset=utf-8');
    header("Content-Disposition: attachment; filename=CSVReport_".$tdate.".csv");
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');


    // output the column headings
    fputcsv($output, array('Ticket ID', 'Fullname','E-Mail','Course URL','Class','Course Name','Program', 
    'Degrees', 'Page','Week','Issue','Tier','Submitted Date', 'Assigned Date', 'Due Date', 
    'User Notes', 'CDT Notes', 'Final Notes', 'File 1','File 2','File 3',
    'Priority','Applied To','Authorized By','Agent','Status','Completed Date', 'PV Codes'));

    include("../../live_connect/connect.inc");
    $query = mysqli_query($conn, $_POST['query']);

    // loop over the rows, outputting them
    while($rows = mysqli_fetch_array($query)) {
        fputcsv($output, array($rows['id'], $rows['fullname'], $rows['email'], $rows['courseurl'], $rows['class'], $rows['cname'], $rows['program'], 
        $rows['degrees'], $rows['page'], $rows['week'], $rows['issue'], $rows['tier'], $rows['submitteddate'],$rows['assigneddate'], $rows['duedate'], 
        $rows['user_note'], $rows['CDT_notes'], $rows['final_notes'], $rows['file1'], $rows['file2'], $rows['file3'], $rows['priority'], 
        $rows['appliedto'], $rows['authorizedby'], $rows['agent'], $rows['status'], $rows['completeddate'], $rows['PVcodes']));
    }
}
?>