<?php
session_start([
    'cookie_lifetime' => 86400,
]); 
include("../includes/security.inc");
security("basic");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Page title</title>
        
        <link rel="stylesheet" href="../includes/styles.css">

        <style>
            .notes p {
                margin-left: 15px;
            }
        </style>

    </head>

    <body id='main_page'>
        <nav class='menu'>
            <?php include_once('../includes/adminmenu.php') ?>
        </nav>

        <content id='content'>
            <div class='headingArea'>Welcome to the Course Reporting Improvement System</div>
            <div class="page">
                <h3>Successfully logged in as Admin.</h3>
                <p>Here is a page by page break down of the ticket system: <p>
                <div class='notes' style='margin-left: 20px;'>
                <?php
                include("../../live_connect/connect.inc");
                $query = mysqli_query($conn, "SELECT * FROM tickets_admin WHERE username = '".$_COOKIE['un']."' ORDER BY username ASC");
                while($rows = mysqli_fetch_array($query)) {
                if($rows['edit'] == 'x') {
                   echo "<h3>Newly Submitted Tickets</h3>
                        <p>
                            The <strong>Newly Submitted Tickets</strong> page is where to go to find any new tickets that have been submitted 
                            to the ticket system that have not been assigned yet. All tickets will appear here and can be accepted.
                        </p>
                    <h3>My Assigned Tickets</h3>
                        <p>
                            The <strong>My Assigned Tickets</strong> page will be where all the tickets that you, as the one assigned to 
                            tickets, have accepted. This is where some ticket
                            information can and should be edited. The CDT Notes section is for your personal notes and the Final Notes section
                            is what the submitter will be able to see from you.
                        </p>";
                }
                if($rows['max'] == 'x') {
                    echo "<h3>Edit Classes</h3>
                        <p>
                            The <strong>Edit Classes</strong> is where
                            admin users can edit information for individual classes. Class information includes the class code, the class
                            name, and any PV codes that are associated with the course. There are instructions for how to add/edit PV codes at the
                            top of the page.
                        </p>
                    <h3>Edit PV codes</h3>
                        <p>
                            The <strong>Edit PV Codes</strong> page is where the PV codes can be modified. Information included on this page 
                            is the PV code itself, the name of
                            the degree associated with the PV code, and the school that the code belongs to.
                        </p>";
                    }
                
                    if($rows['basic'] == 'x') {
                        echo "<h3>Ticket Search</h3>
                        <p>
                            The <strong>Ticket Search</strong> page is where you can search the history of CDT tickets. We ask that if you 
                            are to submit a ticket you make sure that we have not already taken the ticket on from another individual.
                        </p>";
                    }
                    if($rows['max'] == 'x') {
                        echo "<h3>Tickets CSV</h3>
                        <p>
                            Here on the <strong>Tickets CSV</strong> page, you may run a CSV report for tickets from this page. This page 
                            pulls all of the information from the tickets that fall within the date range that you provide.                         
                        </p>";
                    }
                    if($rows['ADC'] == 'x') {
                        echo "<h3>ADC Reports</h3>
                        <p>
                            The <strong>ADC Reports</strong> page is where the ADC will go to create an ADC Report. There are two options 
                            for this report; one may print the report within a certain date range or may print all tickets.
                        </p>";
                    }
                }
                ?>
                </div>
            </div>
        </content>
    </body>
</html>

